import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../header/header.service';
import { UrlService } from '../url.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private serverurl: UrlService, private http: HttpClient, private router: Router, private header: HeaderService) { }

  commonServiceGet(data:any) {
    console.log(data);
    return this.http.get(`${this.serverurl.url4}/`+data.my_url, this.header.getHeaders(data)).toPromise()
  }

  commonServicePost(data:any){
    return this.http.post(`${this.serverurl.url4}/`+data.my_url,data, this.header.getHeader()).toPromise()
  }
  
  commonServicePostWithHeader(data:any){
    console.log(data);
    return this.http.post(`${this.serverurl.url4}/`+data.my_url,data, this.header.getHeaderPost(data)).toPromise()
  }

  getAttendanceList(data:any){
    console.log(data);
    return this.http.post(`${this.serverurl.url4}/`+data.my_url,data, this.header.getattheader()).toPromise()
  }

  MhrEmpAttMapping_List(data:any){
    console.log(data);
    return this.http.post(`${this.serverurl.url4}/`+data.my_url,data, this.header.getattheader()).toPromise()
  }
  MhrEmpAttMappingSave(data:any){
    console.log(data);
    return this.http.post(`${this.serverurl.url4}/`+data.my_url,data, this.header.getattheader()).toPromise()
  }
  MHrEmpunderwork(data:any){
    console.log(data);
    return this.http.get(`${this.serverurl.url4}/`+data.my_url,this.header.getattheader()).toPromise()
  }
  getMhrEmpAttMappingType_List(data:any){
    console.log(data);
    return this.http.get(`${this.serverurl.url4}/`+data.my_url,this.header.getattheader()).toPromise()
  }

  commonUploadImage(data:any){
    console.log(data);
    
    return this.http.post(data.data[0]['serverurl']+data.data[0]['my_url'],data, this.header.getHeader()).toPromise()
  }
}
