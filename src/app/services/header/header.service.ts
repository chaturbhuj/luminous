import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HeaderService {
  constructor() {}
  // httpOptions: any;

  getHeader() {
    return {
      headers: new HttpHeaders({
        key: localStorage.getItem('auth_key') || '',
        userid: localStorage.getItem('userid') || '',
        'content-Type': 'application/json',
      }),
    };
  }

  
  getImageHeader() {
    return {
      headers: new HttpHeaders({
        key: localStorage.getItem('auth_key') || '',
        userid: localStorage.getItem('userid') || '',
        'content-Type': 'application/json',
        
      }),
    };
  }

  getHeaders(data:any) {
    if(data.headerKey == 'HRCode'){
      return {
        headers: new HttpHeaders({
          key: localStorage.getItem('auth_key') || '',
          userid: localStorage.getItem('userid') || '',
          'content-Type': 'application/json',
          'HRCode': data.headervalue,
        }),
      };
    }else if(data.headerKey == 'RequestHistory'){
      return {
        headers: new HttpHeaders({
          key: localStorage.getItem('auth_key') || '',
          userid: localStorage.getItem('userid') || '',
          'content-Type': 'application/json',
          'RequestId': data.headervalue,
          'empcode': data.headervalue1,
        }),
      };
    }else if(data.headerKey == 'WitnessList'){
      return {
        headers: new HttpHeaders({
          key: localStorage.getItem('auth_key') || '',
          userid: localStorage.getItem('userid') || '',
          'content-Type': 'application/json',
          'RequestId': data.headervalue,
          'empcode': data.headervalue1,
        }),
      };
    }else if(data.headerKey == 'empcode'){
      return {
        headers: new HttpHeaders({
          key: localStorage.getItem('auth_key') || '',
          userid: localStorage.getItem('userid') || '',
          'content-Type': 'application/json',
          //'RequestId': data.headervalue,
          'empcode': data.headervalue1,
        }),
      };
    }else if(data.headerKey == 'RMCode'){
      return {
        headers: new HttpHeaders({
          key: localStorage.getItem('auth_key') || '',
          userid: localStorage.getItem('userid') || '',
          'content-Type': 'application/json',
          //'RequestId': data.headervalue,
          'RMCode': data.headervalue,
        }),
      };
    }else if(data.headerKey == 'AdvanceReport'){
      return {
        headers: new HttpHeaders({
          key: localStorage.getItem('auth_key') || '',
          userid: localStorage.getItem('userid') || '',
          'content-Type': 'application/json',
          'empcode': data.empcode,
          'startdate':data.startdate,
          'enddate':data.enddate
        }),
      };
    }else if(data.headerKey == 'attendance_list'){
      return {
        headers: new HttpHeaders({
          key: '123456',
          empcode: localStorage.getItem('userid') || '',
          
        }),
      };
    }else{
      return{};
    }
  }

  getHeaderPost(data:any) {
    if(data.headerKey == 'loan_save_qty'){
      return {
        headers: new HttpHeaders({
          key: localStorage.getItem('auth_key') || '',
          userid: localStorage.getItem('userid') || '',
          'content-Type': 'application/json',
          'loantype':data.loantype,
          'loanpurpose':data.loanpurpose,
          'loanemi':data.loanemi,
          'Amount':data.Amount,
          'Address':data.Address,
          'MobileNo':data.MobileNo,
          'EmpId': data.EmpId,
          'EmpName':"Dummy",
          'Empca':"DVC",
          'Repor':"10995001",
          'Hrss':"10995001|10995002",
          'hod':"",
          'other':data.other,
          'pincode':data.pincode,
          'covidtype':data.covidtype,
          'relation':""
        }),
      };
    }else if(data.headerKey == 'cancel_loan'){
      return {
        headers: new HttpHeaders({
          'content-Type': 'application/json',
          'Id': data.Id,
        }),
      };
    }else{
      return{};
    }
  }

  getattheader(){
    return {
      headers: new HttpHeaders({
        key: '123456' ,
        empcode: localStorage.getItem('userid') || '',
      }),
    };
  }

}
