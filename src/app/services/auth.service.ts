import { Injectable } from '@angular/core';
import { UrlService } from '../services/url.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private serverurl: UrlService,
    private http: HttpClient,
    private router: Router
  ) {}
  getJwt() {
    const token = localStorage.getItem('auth_key');
    if (token) {
      return token;
    } else return null;
  }

  removeJwt() {
    localStorage.removeItem('auth_key');
    this.router.navigateByUrl('/login');
  }

  login(formData: any) {
    return this.http
      .post(`${this.serverurl.url}/api/login/MHrLoginCheck`, formData)
      .toPromise();
  }

  forgetPassword(formData:any){
    return this.http.post(`${this.serverurl.url}/api/login/MHrForgetPassword`, formData).toPromise()
  }

  // forgetPassword2(formData:any){
  //   return this.http.post(`${this.serverurl.url}/api/login/MHrForgetPassword2`, formData, this.httpOptions).toPromise()
  // }

  submitOtp(formData: any) {
    return this.http
      .post(`${this.serverurl.url}/api/login/MHrcheckotp`, formData)
      .toPromise();
  }

  changePassword(formData: any) {
    return this.http
      .post(`${this.serverurl.url}/api/login/MHrLoginchangepassword`, formData)
      .toPromise();
  }
}
