import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../header/header.service';
import { UrlService } from '../url.service';

@Injectable({
  providedIn: 'root',
})
export class DashboardServiceService {
  constructor(
    private serverurl: UrlService,
    private http: HttpClient,
    private router: Router,
    private header: HeaderService
  ) {}

  getDashboardHomeImages() {
    return this.http
      .get(
        `${this.serverurl.url3}/api/DashboardHome/dash_Display_Master_Image?mid=0`,
        this.header.getHeader()
      )
      .toPromise();
  }

  getDashboardClick(mid: any) {
    return this.http
      .get(
        `${this.serverurl.url3}/api/DashboardHome/dash_Display_Other_Image?mid=${mid}`,
        this.header.getHeader()
      )
      .toPromise();
  }

  getDashboardBannerImage() {
    return this.http
      .get(
        `${this.serverurl.url3}/api/DashboardHome/Dash_Banner_Image`,
        this.header.getHeader()
      )
      .toPromise();
  }

  getBirthdayList() {
    return this.http
      .get(
        `${this.serverurl.url3}/api/DashboardHome/Dash_EmpBirthdayList`,
        this.header.getHeader()
      )
      .toPromise();
  }

  Dash_Category_Hdr(data:any) {
    return this.http
      .get(
        `${this.serverurl.url3}/api/DashboardHome/Dash_Category_Hdr?AppName=${data.Appname}`,
        this.header.getHeader()
      )
      .toPromise();
  }

  Dash_Category_Det(data:any) {
    return this.http
      .get(
        `${this.serverurl.url3}/api/DashboardHome/Dash_Category_Det?CatId=${data.CatId}`,
        this.header.getHeader()
      )
      .toPromise();
  }
  
}
