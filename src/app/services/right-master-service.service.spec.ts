import { TestBed } from '@angular/core/testing';

import { RightMasterServiceService } from './right-master-service.service';

describe('RightMasterServiceService', () => {
  let service: RightMasterServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RightMasterServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
