import { HttpClient, HttpHeaderResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from './header/header.service';
import { UrlService } from './url.service';

@Injectable({
  providedIn: 'root'
})
export class HrDashboardService {

  constructor(private serverurl: UrlService, private http: HttpClient, private router: Router, private headerService:HeaderService) { }
  httpOptions = {
    headers: new HttpHeaders({
      'content-Type': 'application/json',
    
    })
  }

  getHeader() {
    return {
      headers: new HttpHeaders({
        key: localStorage.getItem('auth_key') || '',
        empcode: localStorage.getItem('userid') || '',
        'content-Type': 'application/json',
      }),
    };
  }

  getloanAdvance(){
    return this.http.get(`${this.serverurl.url}/api/Loan/LoanSP_list`, this.getHeader()).toPromise()
  }

  saveLoan(formData:any){
    return this.http.post(`${this.serverurl.url}/api/Loan/LoanSaveQty`,formData,this.httpOptions).toPromise()
  }
  
}
