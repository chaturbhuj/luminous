import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../header/header.service';
import { UrlService } from '../url.service';

@Injectable({
  providedIn: 'root'
})
export class ItServiceDeskService {


  constructor(private serverurl: UrlService, private http: HttpClient, private router: Router, private header: HeaderService) { }

  serialize(obj:any) {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

  getSubCategory() {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/SubCategoryList?CatCode=1`, this.header.getHeader()).toPromise()
  }

  getCategory() {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/CategoryList`, this.header.getHeader()).toPromise()
  }

  getLocationList(itemcode: any) {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/LocationList?ItemCode=${itemcode}`, this.header.getHeader()).toPromise()
  }

  getItemList(subcat: any, typeOfRequest:any) {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/ItemList?CatCode=1&SubCat=${subcat}&TypeReq=${typeOfRequest}`, this.header.getHeader()).toPromise()
  }

  checkSucessfactorlogticket(subcat: any) {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/checksucessfactorlogticket?Subcat=${subcat}`, this.header.getHeader()).toPromise()
  }

  getChangeTypeList() {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/ChangeTypeList`, this.header.getHeader()).toPromise()
  }

  getChangedurationList() {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/ChangedurationList`, this.header.getHeader()).toPromise()
  }

  getEmployeeDetails(searchValue: any, searchType: any) {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/Search_Emp?Empcode=${searchValue}&serchtype=${searchType}`, this.header.getHeader()).toPromise()
  }

  checkTypeofRequesSubCatwise(subcat: any) {
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/checkTypeofRequesSubCatwise?Subcat=${subcat}`, this.header.getHeader()).toPromise()
  }

  getRequestypeList(){
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/RequestypeList`, this.header.getHeader()).toPromise()
  }

  getPriorityMasterList() {

   return this.http.get(`${this.serverurl.url2}/api/LogTicket/PriorityMasterList`, this.header.getHeader()).toPromise()

  }

  postTicket(obj:any){
    // let obj:any = {
    //   postname: 'TestWepapi',
    //   postedby: '10995534',
    //   cat: '1',
    //   subcat: '10',
    //   postdesc: '<p>&nbsp;test desccc</p>',
    //   item: '347',
    //   site: '14',
    //   userid: '10995534',
    //   projectid: '0',
    //   flag: '',
    //   ReqType: '2',
    //   AAName: '',
    //   RChange: '',
    //   IChange: '0',
    //   TChange: '0',
    //   TypeIncident: '1',
    //   DateIncident: '',
    //   ImpactIncident: '0',
    //   UrgencyIncident: '0',
    //   DTIncident: '',
    //   LIncident: '',
    //   SrvP: '5',
    //   Changedur: '2',
    //   tempday: '',
    //   Just: '',
    //   VPeriod: '',
    //   Risk: '',
    // };
    return this.http.post(`${this.serverurl.url2}/api/LogTicket/PostTicket`,obj, this.header.getHeader()  ).toPromise()
  }

  addAttachment(obj:any){
    return this.http.post(`${this.serverurl.url2}/api/LogTicket/Addtachment`,obj, this.header.getHeader()  ).toPromise()
  }

  getUserTicketList(){
    return this.http.get(`${this.serverurl.url2}/api/Userservice/UserTicketDeatisl?Cat=0&subcat=0&Stdate=&Enddate=`, this.header.getHeader()).toPromise()
  }

  getTicketDetails(postId:any){
    return this.http.get(`${this.serverurl.url2}/api/Userservice/ShowTicketDetails?postid=${postId}`, this.header.getHeader()).toPromise()
  } 

  CheckChilticket(postId:any){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/CheckChilticket?postid=${postId}`, this.header.getHeader()).toPromise()
  } 

  GetAssignToList(data:any){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/GetAssignToList?postid=${data.postId}&techitem=${data.techitem}`, this.header.getHeader()).toPromise()
  } 

  CheckAssginusermail(data:any){
    console.log(data);
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/CheckAssginusermail?postid=${data.postid}&assginto=${data.assginto}`, this.header.getHeader()).toPromise()
  } 

  getTicketReplyDetails(postId:any){
    return this.http.get(`${this.serverurl.url2}/api/Userservice/ShowTicketReplyDetails?postid=${postId}`, this.header.getHeader()).toPromise()
  } 

  CheckUTSDLC(data:any){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/CheckUTSDLC?status=${data.status}&assignto=${data.assginto}&changetype=${data.changetype}&postid=${data.postid}`, this.header.getHeader()).toPromise()
  } 

  checkTech(){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/checktech`, this.header.getHeader()).toPromise()
  }

  CheckMailRadio(){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/CheckMailRadio`, this.header.getHeader()).toPromise()
  }

  checkTech2(){
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/chk_user_right2?Userid=10995534`).toPromise()
  }
  

  getTotalEmployeeTickets(){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/WebEmployeeTicketTotal`, this.header.getHeader()).toPromise()
  }

  getTechnicianTicketList(formData:any){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/TechTicketDeatisl?${this.serialize(formData)}`, this.header.getHeader()).toPromise()
  }

  selectTechnicianControllerlist(reqType:any){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/selectTechnicianControllerlist?reqytype=${reqType}`, this.header.getHeader()).toPromise()
  }

  getStatusList(){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/Getstatus`, this.header.getHeader()).toPromise() 
  }
  getITEMListTechTicket(){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/ITEMListTechTicket`, this.header.getHeader()).toPromise() 
  }

  getEmpItemList(){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/Getempitem`, this.header.getHeader()).toPromise()
  }

  saveSelectionDay(days:any){
    return this.http.post(`${this.serverurl.url2}/api/Technicianservice/SaveSelDay?days=${days}`,{}, this.header.getHeader()).toPromise()
  }

  getPickupPopup(postid:any){
    return this.http.get(`${this.serverurl.url2}/api/Technicianservice/PickupPopup?postid=${postid}`, this.header.getHeader()).toPromise()
  }

  PostReplyUser(data:any){
    console.log(data);
    return this.http.post(`${this.serverurl.url2}/api/Userservice/PostReplyUser?ticketid=${data.ticketid}&replydesc=${data.replydesc}&ass=${data.ass}`,data, this.header.getHeader()).toPromise()
    // return this.http.post(`${this.serverurl.url2}/api/Userservice/PostReplyUser`,data, this.header.getHeader()).toPromise()
  }
  savereply(data:any){
    console.log(data);
    return this.http.post(`${this.serverurl.url2}/api/Technicianservice/savereply`,data, this.header.getHeader()).toPromise()
  }

  pickupSave(data:any){
    console.log(data);
    return this.http.post(`${this.serverurl.url2}/api/Technicianservice/pickupSave?postid=${data.postid}&Priority=${data.Priority}&impactofchange=${data.impactofchange}&UrgencyCategory=${data.UrgencyCategory}&ImpactCategory=${data.ImpactCategory}`,data, this.header.getHeader()).toPromise()
  }

  ReturnRequestbyAdmin(data:any){
    console.log(data);
    return this.http.post(`${this.serverurl.url2}/api/Technicianservice/ReturnRequestbyAdmin?postid=${data.postid}&reply=${data.reply}`,data, this.header.getHeader()).toPromise()
  }

  GetAddtachment(data:any){
    console.log(data.ticketid);
    return this.http.get(`${this.serverurl.url2}/api/LogTicket/GetAddtachment?ticketid=${data.ticketid}`, this.header.getHeader()).toPromise()

  }
}


