import { TestBed } from '@angular/core/testing';

import { ItServiceDeskService } from './it-service-desk.service';

describe('ItServiceDeskService', () => {
  let service: ItServiceDeskService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItServiceDeskService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
