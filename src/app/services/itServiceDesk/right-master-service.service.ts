import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UrlService } from '../url.service';

@Injectable({
  providedIn: 'root'
})
export class RightMasterServiceService {

  constructor(private serverurl: UrlService, private http: HttpClient, private router: Router) { }
  httpOptions = {
    headers: new HttpHeaders({
      'content-Type': 'application/json',
      'key': 'U0VQVEAySzIx',
      'userid': '10995534'
    })
  }

  getuserrightmaster(val:any){
    return this.http.get(`${this.serverurl.url2}/api/Rights/GridUser?Employee=${val}`, this.httpOptions).toPromise()
  }

}
