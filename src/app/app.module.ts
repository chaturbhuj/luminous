import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserregisterComponent } from './index/userregister/userregister.component';
import { UserloginComponent } from './index/userlogin/userlogin.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './index/dashboard/dashboard.component';
import { MainComponent } from './index/itServiceDesk/main/main.component';
import { HomeComponent } from './index/itServiceDesk/home/home.component';
import { ProjectComponent } from './index/itServiceDesk/project/project.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SideNavComponent } from './index/side-nav/side-nav.component';
import { FormsComponent } from './index/forms/forms.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LogTicketComponent } from './index/itServiceDesk/log-ticket/log-ticket.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import { ShowTicketDetailsComponent } from './index/itServiceDesk/show-ticket-details/show-ticket-details.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { AssignmentComponent } from './index/itServiceDesk/assignment/assignment.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { TechTicketDetailComponent } from './index/itServiceDesk/tech-ticket-detail/tech-ticket-detail.component';
import { RightMasterComponent } from './index/itServiceDesk/right-master/right-master.component';
import { LoanAdvanceRequestComponent } from './index/hrDashboard/loan-advance-request/loan-advance-request.component';
import { HrdashboardComponent } from './index/hrDashboard/hrdashboard/hrdashboard.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { LoanAdvanceApprovalComponent } from './index/hrDashboard/loan-advance-approval/loan-advance-approval.component';
import { DetailModalComponent } from './index/hrDashboard/detail-modal/detail-modal.component';
import { LoanPendingApprovalComponent } from './index/hrDashboard/loan-pending-approval/loan-pending-approval.component';
import { HrDashboardNavbarComponent } from './index/hr-dashboard-navbar/hr-dashboard-navbar.component';
import { AmountToWordPipe } from './amount-to-word.pipe';
import { HrDashboardFooterComponent } from './index/hrDashboard/hr-dashboard-footer/hr-dashboard-footer.component';

import { DashboardMenuComponent } from './index/hrDashboard/dashboard-menu/dashboard-menu.component';

import { MsalModule, MsalService, MSAL_INSTANCE } from '@azure/msal-angular';
import { IPublicClientApplication, PublicClientApplication } from '@azure/msal-browser';
import { LoanAdvanceReportComponent } from './index/hrDashboard/loan-advance-report/loan-advance-report.component';


export function MSALInstanceFactory(): IPublicClientApplication {
  return new PublicClientApplication({
    auth: {
      clientId: '7015a0c6-8b6e-4110-86d4-e1515af726c2',
      redirectUri: 'https://dashtest.luminousindia.com' 
    }
  });
}
import { DataTablesModule } from "angular-datatables";
import { OwnAttendanceComponent } from './index/hrDashboard/own-attendance/own-attendance.component';
import { ReporteeAttendanceComponent } from './index/hrDashboard/reportee-attendance/reportee-attendance.component';
import { AttendanceHRComponent } from './index/hrDashboard/attendance-hr/attendance-hr.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { EmpAttendanceMappingComponent } from './index/hrDashboard/emp-attendance-mapping/emp-attendance-mapping.component';
import { EmpSearchModalComponent } from './index/hrDashboard/emp-search-modal/emp-search-modal.component';
import { AboutCompanyComponent } from './index/Pages/about-company/about-company.component';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ChangePasswordComponent } from './index/Pages/change-password/change-password.component';

@NgModule({
  declarations: [
    AppComponent,
    UserregisterComponent,
    UserloginComponent,
    DashboardComponent,
    MainComponent,
    HomeComponent,
    ProjectComponent,
    SideNavComponent,
    FormsComponent,
    LogTicketComponent,
    ShowTicketDetailsComponent,
    AssignmentComponent,
    TechTicketDetailComponent,
    RightMasterComponent,
    LoanAdvanceRequestComponent,
    HrdashboardComponent,
    LoanAdvanceApprovalComponent,
    DetailModalComponent,
    LoanPendingApprovalComponent,
    HrDashboardNavbarComponent,
    HrDashboardFooterComponent,
    DashboardMenuComponent,
    LoanAdvanceReportComponent,
    OwnAttendanceComponent,
    ReporteeAttendanceComponent,
    AttendanceHRComponent,
    EmpAttendanceMappingComponent,
    EmpSearchModalComponent,
    AboutCompanyComponent,
    ChangePasswordComponent,
    
   
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularEditorModule,
    BrowserAnimationsModule,
  	ToastrModule.forRoot(),
    NgxDaterangepickerMd.forRoot(),
    CarouselModule,
  

    // angular material

    MatSidenavModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    
    MatPaginatorModule,
    NgxSpinnerModule,
    BrowserModule, 
    NgxPaginationModule,
  	DataTablesModule ,
    MatCarouselModule.forRoot(),

  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},AmountToWordPipe,{
      provide: MSAL_INSTANCE,
      useFactory: MSALInstanceFactory
    },
    MsalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
