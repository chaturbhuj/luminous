import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DashboardServiceService } from 'src/app/services/dashboard/dashboard-service.service';

@Component({
  selector: 'app-about-company',
  templateUrl: './about-company.component.html',
  styleUrls: ['./about-company.component.css']
})
export class AboutCompanyComponent implements OnInit {
  cat_id=''
  page_content:any =[]
 
  constructor(private activatedRoute: ActivatedRoute,private spinner: NgxSpinnerService, private dashboardService: DashboardServiceService ) { }

  ngOnInit(): void {

    if(this.activatedRoute.snapshot.queryParams.cat_id){
      this.cat_id = this.activatedRoute.snapshot.queryParams.cat_id;
    }
    this.getPageContent();
  }
  async getPageContent(){
    this.spinner.show();
      let data ={
        CatId:this.cat_id
      }
      const res: any = await this.dashboardService.Dash_Category_Det(data);
      console.log(res);
       this.page_content = res.Detail.data;
       console.log(this.page_content);
      
      this.spinner.hide()
  }
}
