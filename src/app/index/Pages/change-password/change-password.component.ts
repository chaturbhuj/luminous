import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/services/common/common.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  old_password='';
  new_password=''
  confirm_password='';
  userid:any ='';
  constructor(private commonService: CommonService,private toastr: ToastrService,private router:Router) { }

  ngOnInit(): void {
    this.userid = localStorage.getItem('userid');

  }

  async update_password(){
    let isError = false;

    $('.error').addClass('display_none');
    if(!this.old_password){
      isError = true;
      $('.old_password_err').removeClass('display_none').html('Old password is required');

    }

    if(!this.new_password){
      isError = true;
      $('.new_password_err').removeClass('display_none').html('New password is required');

    }

    if(!this.confirm_password){
      isError = true;
      $('.confirm_password_err').removeClass('display_none').html('Confirm password is required');

    }

    if(isError == false){

      let data ={
        my_url: 'api/login/MHrLoginchangepassword',
        "userid": this.userid,
        "oldpwd": this.old_password,
        "newpwd": this.new_password,
        "confirmpwd": this.confirm_password
      }

      let res:any = await this.commonService.commonServicePost(data);
      console.log(res);
      if(res.status == 401){
        // alert(res.Message);
        this.toastr.error('Error!',res.Message);

      }else{
        this.toastr.success('Success!', 'Password Changed successfully.');
        localStorage.clear();
        this.router.navigateByUrl('login');
      }
      
    }
    
  }

  validate_input(input_field:any,err_class:any){
    if($('.'+input_field).val() !== ''){
        $('.'+err_class).addClass('display_none');
    }else{
      $('.'+err_class).removeClass('display_none');
    }
  } 
}
