import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { CommonService } from 'src/app/services/common/common.service';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-emp-search-modal',
  templateUrl: './emp-search-modal.component.html',
  styleUrls: ['./emp-search-modal.component.css']
})
export class EmpSearchModalComponent implements OnInit {
  employeeList: any;

  constructor(private itServiceDesk: ItServiceDeskService, private fb: FormBuilder,private commonService: CommonService, private toastr: ToastrService ) { }
  employeeSearchForm: FormGroup;

  ngOnInit(): void {
    this.employeeSearchForm = this.fb.group({
      searchType: [1],
      searchValue: ['', Validators.required]
    })
  }

  get searchValue() {
    return this.employeeSearchForm.get('searchValue')
  }

  get searchType() {
    return this.employeeSearchForm.get('searchType')
  }

  async searchEmployee() {
    if (this.searchValue?.value == '') {
      return;
    }
    // console.log(this.employeeSearchForm.value)
    const res: any = await this.itServiceDesk.getEmployeeDetails(this.searchValue?.value, this.searchType?.value)
    this.employeeList = res.Detail.data;
    console.log(this.employeeList)
  }

  setEmpCode(obj:any){}

}
