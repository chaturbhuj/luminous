import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpSearchModalComponent } from './emp-search-modal.component';

describe('EmpSearchModalComponent', () => {
  let component: EmpSearchModalComponent;
  let fixture: ComponentFixture<EmpSearchModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpSearchModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpSearchModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
