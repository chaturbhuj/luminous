import { Component, OnInit,Input,SimpleChanges , Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as converter from 'number-to-words';
import { CommonService } from 'src/app/services/common/common.service';

declare var $:any
@Component({
  selector: 'app-detail-modal',
  templateUrl: './detail-modal.component.html',
  styleUrls: ['./detail-modal.component.css']
})
export class DetailModalComponent implements OnInit {
  @Input() loanApprovalHistory:any = [];
  @Input() witness_accept_array:any =[];
  @Input() witness_list_array:any =[];
  
  @Output() is_witness_form_submitted = new EventEmitter<boolean>();

  lblPayNo: any;
  today =''
  amount_in_words = ''
  emi_months:any =''
  installments:any =''
  witness1=''
  witness2=''
  accept_w_form = '';
  view_agreement = false;
  is_disabled= false;

  constructor(private commonService: CommonService,  private toastr: ToastrService,) { }
 

  ngOnInit(): void {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

      this.today = dd + '/' + mm + '/' + yyyy;
      console.log(this.today);
    let self = this;


    console.log(this.witness_accept_array);
    
    if(this.witness_accept_array.length)
    {
    console.log(this.witness_accept_array);
      
    }
  
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    //console.log(changes.witness_accept_array.currentValue)
    //console.log(this.witness_accept_array);
    //this.witness_accept_array = changes.witness_accept_array.currentValue;

    console.log(this.witness_accept_array);
    
    if(this.witness_accept_array.CategoryType=='Loan' && this.witness_accept_array.Status=='9' && this.witness_accept_array.Witness=='3'){
      // $('.witness1').prop('disabled',true);
      this.is_disabled =true;
      this.witness1 = this.witness_accept_array.firstwitness;
      this.view_agreement=false;
      
    }

    if(this.witness_accept_array.CategoryType=='Loan' && this.witness_accept_array.Status=='10'){
      this.view_agreement =true;
    }

 
    
    if(this.witness_accept_array.Amount){
      var amount = this.witness_accept_array.Amount;
      var amount_array = amount.split('.');
      console.log(amount_array.length);
      console.log(amount_array);
      if(amount_array.length ==2 && amount_array[1]=='00'){
        var rupees = converter.toWords(amount_array[0]) +' rupees only';
        this.amount_in_words = this.capitalize(rupees);
      }else if(amount_array.length==2 && amount_array[1]!=='00'){
        var rupees = converter.toWords(amount_array[0]) +' rupees';
        var paise =  converter.toWords(amount_array[1]) +" paise only";
        this.amount_in_words = this.capitalize(rupees)+ ' and '+ this.capitalize(paise);
      }
      console.log(this.amount_in_words);
    
      console.log(this.witness_accept_array);
      this.emi_months =this.witness_accept_array.Emitype.split(' ')[0];
      console.log(this.emi_months);
      this.installments = (this.witness_accept_array.Amount/this.emi_months).toFixed(2);;
    }
  }

  capitalize(txt:any) {
    return txt.charAt(0).toUpperCase() + txt.slice(1).toLowerCase();
  }
  
  async witness_submit(){
    let isError = false;

    if(!this.witness1){
      isError = true
      $('.witness1_error').removeClass('display_none');
    }

    if(!this.witness2){
      isError = true
      $('.witness2_error').removeClass('display_none');
    }

    if(!this.accept_w_form){
      isError = true
      $('.accept_w_form_error').removeClass('display_none');
    }
     
    if(isError == false){
      $('.accept_w_form_error').addClass('display_none');
      let data1 ={
        my_url : 'api/Loan/WitnessSubmit',
        Id: this.witness_accept_array.RequestID,
        witness1 : this.witness1,
        witness2 : this.witness2,
      }
      const res: any = await this.commonService.commonServicePost(data1);
      console.log(res);
      this.toastr.success('Success!', 'Sbumitted successfully.');
      $('#witness_accept_modal').modal('hide');
      this.witness1=''
      this.witness2=''
      this.accept_w_form=''
      this.is_witness_form_submitted.emit(true);
    }
  }

  validate_input(input_field:any,err_class:any){
    if($('.'+input_field).val() !== ''){
        $('.'+err_class).addClass('display_none');
    }else{
      $('.'+err_class).removeClass('display_none');
    }
  }

  print_agreement():void{

    // var divContents = $("#ContentPlaceHolder1_pnlAggreement").innerHTML;
    // console.log(divContents);
    // var a = window.open('');
    // a?.document.write('<html>');
    // a?.document.write('<body > <h1>Div contents are <br>');
    // a?.document.write(divContents);
    // a?.document.write('</body></html>');
    // a?.document.close();
    // a?.print();

    (window as any).print();
    $('.print_agreemnet').trigger('click')
  }

  
}
