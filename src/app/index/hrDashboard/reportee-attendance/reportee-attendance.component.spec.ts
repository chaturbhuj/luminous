import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteeAttendanceComponent } from './reportee-attendance.component';

describe('ReporteeAttendanceComponent', () => {
  let component: ReporteeAttendanceComponent;
  let fixture: ComponentFixture<ReporteeAttendanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteeAttendanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteeAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
