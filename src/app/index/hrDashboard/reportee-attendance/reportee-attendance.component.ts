import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common/common.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-reportee-attendance',
  templateUrl: './reportee-attendance.component.html',
  styleUrls: ['./reportee-attendance.component.css']
})
export class ReporteeAttendanceComponent implements  AfterViewInit, OnDestroy,OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective | undefined;
  isDtInitialized = 'no';dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  filter_by='1'

  year_arr:any =[]
  year:any='';
  month ='01'
  from_date=''
  to_date=''
  attendance_list:any=[]
  userid:any =''
  employee_list:any =[]
  employee_code:any =''
  
  constructor(private commonService: CommonService,private spinner: NgxSpinnerService) { }
  

  ngOnInit(): void {
    let current_year = (new Date()).getFullYear();
    this.year = current_year;
    let last_year = current_year-1;
    this.year_arr =[
      current_year,
      last_year
    ]
    console.log(current_year);
    this.userid = localStorage.getItem('userid');
    this.dtOptions = {
		  pagingType: 'full_numbers',
		  pageLength: 100,
			dom: 'Bfrtip',
      bFilter:false,
			buttons: [],
			responsive: true
		};
    this.MHrEmpunderwork();
  }
  ngAfterViewInit() {
		
	}
  reset(){
    window.location.reload();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
	rerender(): void {
    console.log("dtInstance",this.isDtInitialized)
		if (this.isDtInitialized == 'yes') {
      console.log("dtInstance",this.dtElement)
		  this.dtElement?.dtInstance.then((dtInstance: any) => {
        console.log("dtInstance",dtInstance)
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		  });
		} else {
        this.isDtInitialized = 'yes';
        this.dtTrigger.next();
      }
	}
	initialize_dtOptions(){ 
		this.rerender();
	}


  filter_month_wise(){
    let isError = false;

    $('.error').addClass('display_none');

    if(!this.month){
      isError = true;
      $('.month_err').removeClass('display_none').html('select month');
    }
    if(!this.year){
      isError=true;
      $('.year_err').removeClass('display_none').html('select year');
    }
    if(!this.employee_code){
      isError=true;
      $('.employee_code_err').removeClass('display_none').html('required');
    }

    if(isError == false){
      this.getAttendance();
    }
  }

  async MHrEmpunderwork(){
    let data = {
      my_url: 'api/att/MHrEmpunderwork',
    }
    const res: any = await this.commonService.MHrEmpunderwork(data);
    console.log(res);
    this.employee_list = res.Detail.EntryListtdata;
  }

  async getAttendance(){
    this.spinner.show();
    let data = {
      my_url: 'api/att/MHrAttShow',
      empcode: this.employee_code,
      month: this.month,
      year: this.year,
      fromdate: '',
      todate:'',
      page: "rm",
      flag: "",
    }

    const res: any = await this.commonService.getAttendanceList(data);
    console.log(res);
    this.attendance_list = res.Detail.EntryListtdata;
    if(this.attendance_list.length>0){
      //this.dtTrigger.distroy();
      this.initialize_dtOptions();
      //this.dtTrigger.next();
    }
    this.spinner.hide();
  }
}
