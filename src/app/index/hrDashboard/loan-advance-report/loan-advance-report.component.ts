import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { UrlService } from 'src/app/services/url.service';
import { DataTableDirective } from 'angular-datatables';
import * as Moment from 'moment';

declare var $:any;

@Component({
  selector: 'app-loan-advance-report',
  templateUrl: './loan-advance-report.component.html',
  styleUrls: ['./loan-advance-report.component.css']
})
export class LoanAdvanceReportComponent implements   AfterViewInit, OnDestroy, OnInit {
	@ViewChild(DataTableDirective)
  dtElement: DataTableDirective | undefined;
  isDtInitialized = 'no';dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  userid:any =''
  loan_report_list:any =[]
  p: number = 1;
  start_date:any =''
  end_date:any =''
  empcode:any ='';
  selected:any={startDate: Moment, endDate: Moment}
  from_date='';
  to_date='';

  constructor(
    private fb: FormBuilder,
    private commonService: CommonService,
    private router: Router,
    private toastr: ToastrService,
    private serverurl: UrlService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.userid = localStorage.getItem('userid');
    //this.getLoanAdvanceReport();
    //this.initialize_dtOptions();
    this.dtOptions = {
		  pagingType: 'full_numbers',
		  pageLength: 10,
			dom: 'Bfrtip',
			buttons: [
			'copy',
			'print',
			'excel',
		  ],
			responsive: true
		};
  }
  
  validate_date(){
    if(this.selected.start !== undefined || this.selected.end !== undefined){
      $('.from_date_err').addClass('display_none');
    }   
  }
	ngAfterViewInit() {
		
	}
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
	rerender(): void {
    console.log("dtInstance",this.isDtInitialized)
		if (this.isDtInitialized == 'yes') {
      console.log("dtInstance",this.dtElement)
		  this.dtElement?.dtInstance.then((dtInstance: any) => {
        console.log("dtInstance",dtInstance)
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		  });
		} else {
        this.isDtInitialized = 'yes';
        this.dtTrigger.next();
      }
	}
	initialize_dtOptions(){ 
		this.rerender();
	}

  async getLoanAdvanceReport(){
    let data ={
      my_url : 'api/Loan/Loan_AdvanceReport',
      headerKey : 'AdvanceReport',
      empcode : this.empcode,
      startdate:this.from_date,
      enddate:this.to_date
    }
    this.spinner.show();
    const res: any = await this.commonService.commonServiceGet(data);
    console.log(res);

    this.loan_report_list =res.Detail.LoanAdvancedata;
    if(this.loan_report_list.length>0){
      this.initialize_dtOptions();
    }
    
    this.spinner.hide();
  }

  selectFiles($event:any){

  }

  approve_loan(){}

  filter_data(){
    console.log("empcode length",this.empcode.length);
    $('.error').addClass('display_none');
    let isError = false;
    if(!this.empcode){
      isError = true;
      $('.empcode_err').removeClass('display_none');
      return
    }else if(this.empcode.length<8){
      isError = true;
      $('.empcode_err').removeClass('display_none').html('Empcode must of 8 digits');
      return
    }

    if(this.selected.start!==undefined || this.selected.end!==undefined){
      //isError = true;
      //$('.from_date_err').removeClass('display_none').html('select date');
      let date3 = new Date(this.selected.start);
      this.from_date = `${('0' +date3.getDate()).slice(-2)}-${('0'+ (date3.getMonth() + 1)).slice(-2)}-${date3.getFullYear()}`;
      
      let date = new Date(this.selected.end);
      this.to_date = `${('0'+date.getDate()).slice(-2)}-${('0'+ (date.getMonth() + 1)).slice(-2)}-${date.getFullYear()}`;
    }
      
     
    
   
    console.log(this.start_date);
    console.log(this.start_date);
    console.log(this.end_date);
    if(isError == false){
      this.getLoanAdvanceReport();
    }
    
  }
}
