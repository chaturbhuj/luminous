import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanAdvanceReportComponent } from './loan-advance-report.component';

describe('LoanAdvanceReportComponent', () => {
  let component: LoanAdvanceReportComponent;
  let fixture: ComponentFixture<LoanAdvanceReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanAdvanceReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanAdvanceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
