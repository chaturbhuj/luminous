import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceHRComponent } from './attendance-hr.component';

describe('AttendanceHRComponent', () => {
  let component: AttendanceHRComponent;
  let fixture: ComponentFixture<AttendanceHRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttendanceHRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceHRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
