
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common/common.service';
import * as Moment from 'moment';
import { Subject } from 'rxjs';
import { UrlService } from 'src/app/services/url.service';
import { DataTableDirective } from 'angular-datatables';


@Component({
  selector: 'app-attendance-hr',
  templateUrl: './attendance-hr.component.html',
  styleUrls: ['./attendance-hr.component.css']
})
export class AttendanceHRComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective | undefined;
  isDtInitialized = 'no';dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  filter_by='1'

  year_arr:any =[]
  year:any='';
  month ='01'
  from_date=''
  to_date=''
  attendance_list:any=[]
  userid:any =''
  employee_code:any =''
  selected:any={startDate: Moment, endDate: Moment}

  
  constructor(private commonService: CommonService,private spinner: NgxSpinnerService) { }


  ngOnInit(): void {
    this.userid = localStorage.getItem('userid');

    let current_year = (new Date()).getFullYear();
    this.year = current_year;
    let last_year = current_year-1;
    this.year_arr =[
      current_year,
      last_year
    ]
    this.dtOptions = {
		  pagingType: 'full_numbers',
		  pageLength: 100,
			dom: 'Bfrtip',
      bFilter:false,
			buttons: [],
			responsive: true
		};
    console.log(current_year);
  }
  ngAfterViewInit() {
		
	}
  reset(){
    window.location.reload();
  }

  initialize_dtOptions(){
    this.rerender()
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

	rerender(): void {
    console.log("dtInstance",this.isDtInitialized)
		if (this.isDtInitialized == 'yes') {
      console.log("dtInstance",this.dtElement)
		  this.dtElement?.dtInstance.then((dtInstance: any) => {
        console.log("dtInstance",dtInstance)
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		  });
		} else {
        this.isDtInitialized = 'yes';
        this.dtTrigger.next();
      }
	}

  async getAttendance(){
    let data = {
      my_url: 'api/att/MHrAttShow',
      headerkey: 'attendance_list',
      empcode: this.employee_code,
      month: this.month,
      year: this.year,
      fromdate: this.from_date,
      todate: this.to_date,
      page: "hr",
      flag: "",
    }
    this.spinner.show();
    const res: any = await this.commonService.getAttendanceList(data);
    console.log(res);
    if(res.status == 401){
      alert(res.Message);
    }else{
      this.attendance_list = res.Detail.EntryListtdata;
      if(this.attendance_list.length>0){
        //this.dtTrigger.distroy();
        this.initialize_dtOptions();
        //this.dtTrigger.next();
      }
    }
  
    this.spinner.hide();
  }

 async filter_month_wise(){
    let isError = false;
    

    $('.error').addClass('display_none');

    if(!this.month){
      isError = true;
      $('.month_err').removeClass('display_none').html('select month');
    }
    if(!this.year){
      isError = true
      $('.year_err').removeClass('display_none').html('select year');
    }
    if(!this.employee_code){
      isError = true
      $('.employee_code_err').removeClass('display_none').html('Employee code required');
    }

    if(isError == false){
      this.getAttendance();
    }
  }
  
  async filter_date_wise(){
    let isError = false;
    $('.error').addClass('display_none');

    console.log(this.selected);
    console.log(this.selected.end);
    console.log(this.selected.start);

    if(this.selected.start==undefined || this.selected.end==undefined){
      isError = true;
      $('.from_date_err').removeClass('display_none').html('select date');
    }
    
    let date3 = new Date(this.selected.start);
    this.from_date = `${('0'+ (date3.getMonth() + 1)).slice(-2)}/${('0' +date3.getDate()).slice(-2)}/${date3.getFullYear()}`;
    
    let date = new Date(this.selected.end);
    this.to_date = `${('0'+ (date.getMonth() + 1)).slice(-2)}/${('0'+date.getDate()).slice(-2)}/${date.getFullYear()}`;

    console.log(date);
    console.log(this.from_date);
    console.log(this.to_date);
   

    if(!this.employee_code){
      isError=true
      $('.employee_code_err').removeClass('display_none').html('Employee code required');
    }else if(this.employee_code.length !== 8){
      isError=true
      $('.employee_code_err').removeClass('display_none').html('Employee code must be of 8 digits');
    }

    if(isError == false){
      this.getAttendance();
    }
  }

  validate_date(event:any){
    console.log(event);
    if(this.selected.start !== undefined || this.selected.end !== undefined){
      $('.from_date_err').addClass('display_none');
    }    
  }

  validate_employee_code(){
    //console.log(this.employee_code);
    if(!this.employee_code){
      
      $('.employee_code_err').removeClass('display_none').html('Employee code required');
    }else if(this.employee_code.length !== 8){
    
      $('.employee_code_err').removeClass('display_none').html('Employee code must be of 8 digits');
    }else{
      $('.employee_code_err').addClass('display_none');
      
    }
  }

}
