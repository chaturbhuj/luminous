import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrDashboardService } from 'src/app/services/hr-dashboard.service';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
declare var $:any;
@Component({
  selector: 'app-loan-advance-request',
  templateUrl: './loan-advance-request.component.html',
  styleUrls: ['./loan-advance-request.component.css']
})

export class LoanAdvanceRequestComponent implements OnInit {
	dtOptions: any = {};
	 dtTrigger: Subject<any> = new Subject<any>();
  loanApprovalHistory: any;
  lblPayNo: any;
  // itServiceDesk: any;

  constructor( private toastr: ToastrService,private service: HrDashboardService,private itServiceDesk:ItServiceDeskService, private commonService: CommonService,private router: Router,private spinner: NgxSpinnerService) { }
  attachedFiles: any;
  showDetailFlag:boolean = false;
  loan: any;
  purpose: any;
  detailData:any;
  //personalDetailForm:FormGroup;
  tablegridData:any=[];
  statusArr:any=[]
  loanflag:boolean = false;
  salaryFlag:boolean = true;
  userid:any =''
  Address=''
  pincode=''
  MobileNo=''
  loantype=''
  loanpurpose=''
  monthCtc:any=''
  maxEligibleAmount:any=''
  Amount:any=''
  intrestAmount:any=''
  loanemi='1'
  other=''
  covidtype=''
  Emidata=''
  isDisabled:boolean =true
  emiarray:any =[];
  file:any =''
  uploaded_file:any=[]
  witness_accept_array:any =[]
  is_witness_form_submitted:boolean = false
  relation=''
  witness_list_array:any =[]
  p: number = 1;
  applied_amt_counter =0;
 


  ngOnInit(): void {
    this.userid =localStorage.getItem('userid');
    this.getloanpurpose();
	
	this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
	  dom: 'Bfrtip',
	  buttons: [
        'copy',
        'print',
        'excel',
      ],
	  responsive: true
    };
	
	
  }
   ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  change_loan_type(){
    console.log("changed loan");
    console.log(this.loantype)
    if(this.loantype == '1'){
      this.loanflag = true;
      this.salaryFlag = false;
      this.applied_amt_counter=0;
      this.reset_variables();
    } else if(this.loantype == '2'){
      this.loanflag = false;
      this.salaryFlag = true;
      this.reset_variables();
    }
    this.validate_input('loantype','loantype_err')
  }

  reset_variables(){
    this.Amount='';
    this.covidtype ='';
    this.loanpurpose=''
    this.monthCtc =''
    this.maxEligibleAmount=''
    this.intrestAmount=''
    this.Address ='';
    this.pincode='';
    this.MobileNo=''
    this.loanemi=''
    this.covidtype =''
    this.relation=''
    this.uploaded_file=''
    $('.error').addClass('display_none');
  }
  
  validate_input(input_field:any,err_class:any){
    if($('.'+input_field).val() !== ''){
      $('.'+err_class).addClass('display_none');
    }else{
      $('.'+err_class).removeClass('display_none');
    }
  }

  onchangeloanpurpose($event:any){
   //this.reset_variables();
    this.calc_interest_amount();    
  }



  calc_max_eligibility(){
    if(this.monthCtc){
      this.maxEligibleAmount = this.monthCtc*3;
      this.validate_input('monthCtc','monthCtc_err')
    }else{
      this.maxEligibleAmount=''
    }
   
  }

  calc_interest_amount(){
    if(this.loanpurpose=='8' && this.Amount){
      this.intrestAmount =0
    }else if(this.Amount){  
      this.intrestAmount = this.Amount*5/100
    }else{
      this.intrestAmount=''
    }
    console.log(this.intrestAmount);
    
  }

  check_applied_amount(){
    this.calc_interest_amount();
    if(this.loantype=='1'){
      if(!this.monthCtc){
        $('.amount_check_err').addClass('display_none')
      }else{
        if(this.Amount > this.maxEligibleAmount){
          $('.amount_check_err').removeClass('display_none').html("Applied amount cannot be greater than max eligible amount")
        }else{
          $('.amount_check_err').addClass('display_none')
        }
      }
    
    }else if(this.loantype=='2'){
        if(this.applied_amt_counter == 0){
          alert('Please ensure applied amount is less than your monthly salary');
        }
        this.applied_amt_counter++;
    }
   
  }

  check_monthctc(){
    this.calc_max_eligibility();
    if(!this.monthCtc){
      this.Amount=''
      this.intrestAmount=''
      this.maxEligibleAmount=''
      $('.amount_check_err').addClass('display_none')
    }else{
      console.log(this.monthCtc);
      console.log(this.maxEligibleAmount);
      console.log(this.Amount);
      if(this.Amount>this.maxEligibleAmount){
        $('.amount_check_err').removeClass('display_none').html("Applied amount cannot be greater than max eligible amount")
      }else{
        $('.amount_check_err').addClass('display_none')

      }
    }
  }
    
  async selectFiles(event: any) {
    this.validate_input('uploaded_file','uploaded_file_err')
    this.attachedFiles = event.target.files[0];
    console.log(this.attachedFiles);
    // var image = this.getBase64(this.attachedFiles);
    // console.log(image);
    await this.getBase64(this.attachedFiles, (result:any) => {
      console.log(result);
      this.upload_file(result,this.attachedFiles.name)
    });
  }

  getBase64(file:any, cb:any) {
		return new Promise(resolve => {
			let reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				resolve(cb(reader.result));
			};
			reader.onerror = function (error) {
				//console.log('Error: ', error);
			};
		});
	}
  
  back(){
    this.showDetailFlag = false;
  }

  async getloanpurpose(){
    this.spinner.show();
    let data ={
      my_url : 'api/Loan/LoanSP_list',
      headerKey : 'empcode',
      headervalue1:this.userid
    }
    const res: any = await this.commonService.commonServiceGet(data);
    console.log(res);
    this.purpose = res.Detail.purposedata;
    this.Emidata = res.Detail.Emidata;
    this.tablegridData= res.Detail.Griddata
    this.detailData = res.Detail.Detaildata[0];
    let self = this;
    var result = Object.keys(self.Emidata[0]).map((key:any) => [(key), self.Emidata[0][key]]);
    result.forEach(element => {
        console.log(element);
        if(element[0] !== 'Month1'){
          this.emiarray.push({
            key: element[0],
            value: element[1]
          })
        }
       
    });
	this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
	  dom: 'Bfrtip',
	  buttons: [
        'copy',
        'print',
        'excel',
      ],
	  responsive: true
    };
	  this.dtTrigger.next();
    // console.log(result);
    // console.log(this.Emidata);
    // console.log(this.emiarray);
    this.spinner.hide();
  }

  async upload_file(file:any,name:any){
    var base64result = file.split(',')[1];
    console.log(file);
    console.log(name);
    let data ={
      my_url : 'api/Loan/LoanAddAttachment',
      headerKey : 'loan_attachment',
      empcode:this.userid,
      file:name,
      img1:base64result,
      LoanType:'1'
    }
    console.log(data);
    
    const res: any = await this.commonService.commonServicePost(data);
    console.log(res);
    this.attachedFiles='';
    this.uploaded_file= res.Detail.Attachmentdata;
    $('.uploaded_file').val('');
  }


  async submit_loan_advance_request(){
    console.log("function called");
    
    let isError = false;
    $('.error').addClass('display_none');

    if(!this.loantype){
      isError =true;
      $('.loantype_err').removeClass('display_none');
    }

    if(!this.loanpurpose){
      isError =true;
      $('.loanpurpose_err').removeClass('display_none');
    }

    if(this.loanpurpose=='7'){
      if(!this.other){
        isError =true;
        $('.other_err').removeClass('display_none');
      }
    }
    
    if(this.loanpurpose=='8'){
      if(!this.covidtype){
        isError =true;
        $('.covidtype_err').removeClass('display_none');
      }
    }

    if(!this.Amount){
      isError =true;
      $('.Amount_err').removeClass('display_none');
    }else{
      if(this.Amount >this.maxEligibleAmount && this.loantype=='1'){
        $('.amount_check_err').removeClass('display_none').html("Applied amount cannot be greater than max eligible amount")
        isError =true
      }
    }

    if(this.covidtype=='2'){
      if(!this.relation){
        isError =true;
        $('.relation_err').removeClass('display_none');
      }
    }

    if(this.loantype =='1'){
      if(!this.loanemi){
        isError =true;
        $('.loanemi_err').removeClass('display_none');
      }
   
      if(!this.monthCtc){
        isError =true;
        $('.monthCtc_err').removeClass('display_none');
      }
      console.log(this.upload_file.length);
      
      if(!this.uploaded_file.length){
        isError =true;
        $('.uploaded_file_err').removeClass('display_none');
      }
      
    }

    if(!this.Address){
      isError =true;
      $('.Address_err').removeClass('display_none');
    }

    if(!this.pincode){
      isError =true;
      $('.pincode_err').removeClass('display_none');
    }

    if(!this.MobileNo){
      isError =true;
      $('.MobileNo_err').removeClass('display_none');
    }
    
    if(this.loantype=='2'){
      this.loanemi = '1';
    }
    console.log(this.intrestAmount);
    
    console.log(isError);
    if(isError == false){
      let data ={
        my_url : 'api/Loan/LoanSaveQty',
        headerKey : 'loan_save_qty',
        loantype:this.loantype,
        loanpurpose:this.loanpurpose,
        loanemi:this.loanemi,
        Amount:this.Amount,
        Address:this.Address,
        MobileNo:this.MobileNo,
        EmpId: this.userid,
        EmpName:"Dummy",
        Empca:"DVC",
        Repor:"10995001",
        Hrss:"10995001|10995002",
        hod:"",
        other:'dmmy',
        pincode:this.pincode,
        covidtype:this.covidtype,
        relation:this.relation
      }
      console.log(data);
    	$('.awaiting').prop('disabled',true);
    	$('.awaiting').html('Please wait..<span class="spinner-border spinner-border-lg" role="status" aria-hidden="true" style="height: 17px;width: 17px;"></span>');
      console.log(this.loanemi);
      const res: any = await this.commonService.commonServicePostWithHeader(data);
      console.log(res);
      if(res.status == 200){
        this.toastr.success('Success!', 'Submitted successfully.');
        this.loantype=''
        this.monthCtc=''
        this.Address=''
        this.pincode=''
        this.MobileNo=''
        this.loantype=''
        this.loanpurpose=''
        this.monthCtc=''
        this.maxEligibleAmount=''
        this.Amount=''
        this.intrestAmount=''
       // this.loanemi='1'
        this.other=''
        this.covidtype=''
        this.Emidata=''
        this.isDisabled =true
        this.emiarray =[];
        this.file =''
        this.loanemi=''
        this.getloanpurpose();
        this.uploaded_file=[]
        $('.awaiting').prop('disabled',false);
				$('.awaiting').html('Save');
      }
    }
  }

  if_witness_form_submitted($event:any){
    if($event==true){this.getloanpurpose()}
  }

  async open_detail_page(data:any){
    console.log(data);
    console.log(data.RequestID);
    console.log(data['RequestID']);
    //window.open(this.serverurl.Baseurl+'loanAdvanceHistory?RequestId='+data.ID+'&empcode='+data.PersonalId, "_blank");
    this.spinner.show();
    let data1 ={
      my_url : 'api/Loan/LoanHistory',
      headerKey : 'RequestHistory',
      headervalue : data.RequestID,
      headervalue1 : data.PersonalId,
    }
    const res: any = await this.commonService.commonServiceGet(data1);
    console.log(res);
    if(res.Detail){
      this.loanApprovalHistory = res.Detail;
      if(this.loanApprovalHistory?.historyDetaildata?.length){
        this.lblPayNo = this.loanApprovalHistory?.historyDetaildata[0]?.lblPayNo;
      }
      
      $('#detail_modal').modal('show');
    }
    this.spinner.hide();
  }

  async cancel_loan(requestId:any)
  {
    console.log(requestId);
    if(confirm('Are you sure, You want to cancel')){
      console.log("sdj");
      let data1 ={
        my_url : 'api/Loan/Usercancel',
        headerKey : 'cancel_loan',
        Id : requestId,
      }
      const res: any = await this.commonService.commonServicePostWithHeader(data1);
      this.toastr.success('Success!', 'Cancelled successfully.');
      console.log(res);
      this.getloanpurpose();
    }
  }

  delete_uploaded_file(index:any){
    console.log("deleted");
    console.log(index);
    //this.uploaded_file=[]
    this.uploaded_file.splice(index, 1);
  }

  async witness_and_accept_digitally(obj:any){
    console.log("witness and accept");
    console.log(obj);
    this.witness_accept_array=obj;
    let data1 ={
      my_url : 'api/Loan/LoanWitness_list',
      headerKey : 'WitnessList',
      headervalue : obj.RequestID,
      headervalue1 : obj.PersonalId,
    }
    const res: any = await this.commonService.commonServiceGet(data1);
    console.log(res);
    this.witness_list_array = res.Detail.witnessdata[0];
    console.log(this.witness_list_array);
    
    $('#witness_accept_modal').modal('show');
    console.log("witness form submitted");
    
  }
}
