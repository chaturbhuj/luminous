import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanAdvanceRequestComponent } from './loan-advance-request.component';

describe('LoanAdvanceRequestComponent', () => {
  let component: LoanAdvanceRequestComponent;
  let fixture: ComponentFixture<LoanAdvanceRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanAdvanceRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanAdvanceRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
