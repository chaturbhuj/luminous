import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanPendingApprovalComponent } from './loan-pending-approval.component';

describe('LoanPendingApprovalComponent', () => {
  let component: LoanPendingApprovalComponent;
  let fixture: ComponentFixture<LoanPendingApprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanPendingApprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanPendingApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
