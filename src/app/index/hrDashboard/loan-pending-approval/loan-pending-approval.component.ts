import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrDashboardService } from 'src/app/services/hr-dashboard.service';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

declare var $:any;

@Component({
  selector: 'app-loan-pending-approval',
  templateUrl: './loan-pending-approval.component.html',
  styleUrls: ['./loan-pending-approval.component.css']
})
export class LoanPendingApprovalComponent implements AfterViewInit, OnDestroy,OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective | undefined;
  isDtInitialized = 'no';dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  tablegridData:any =[]
  loanApprovalHistory:any =[]
  lblPayNo=''
  loanApprovalList: any;
  userid: any;
  

  constructor( private toastr: ToastrService,private service: HrDashboardService,private itServiceDesk:ItServiceDeskService, private commonService: CommonService,private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
      this.userid = localStorage.getItem('userid');
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
        dom: 'Bfrtip',
        buttons: [
        'copy',
        'print',
        'excel',
        ],
        responsive: true
      };
      this.getPendingLoanlist();
    
  }
  ngAfterViewInit() {
		
	}
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
	rerender(): void {
    console.log("dtInstance",this.isDtInitialized)
		if (this.isDtInitialized == 'yes') {
      console.log("dtInstance",this.dtElement)
		  this.dtElement?.dtInstance.then((dtInstance: any) => {
        console.log("dtInstance",dtInstance)
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		  });
		} else {
        this.isDtInitialized = 'yes';
        this.dtTrigger.next();
      }
	}
	initialize_dtOptions(){ 
		this.rerender();
	}

  async getPendingLoanlist() {
    this.spinner.show();
    let data ={
      my_url : 'api/Loan/LoanProcessing_list',
      headerKey : 'RMCode',
      headervalue : this.userid,
    }
    const res: any = await this.commonService.commonServiceGet(data);
    console.log(res);
    this.loanApprovalList = res.Detail.RMdata;
    if(this.loanApprovalList.length>0){
      //this.dtTrigger.distroy();
      this.initialize_dtOptions();
      //this.dtTrigger.next();
    }
    this.spinner.hide();
  }

  async open_detail_page(data:any){
    //console.log(data);
    //window.open(this.serverurl.Baseurl+'loanAdvanceHistory?RequestId='+data.ID+'&empcode='+data.PersonalId, "_blank");
    this.spinner.show();
    let data1 ={
      my_url : 'api/Loan/LoanHistory',
      headerKey : 'RequestHistory',
      headervalue : data.ID,
      headervalue1 : data.PersonalId,
    }
    const res: any = await this.commonService.commonServiceGet(data1);
    console.log(res);
    if(res.Detail){
      this.loanApprovalHistory = res.Detail;
      if(this.loanApprovalHistory?.historyDetaildata?.length){
        this.lblPayNo = this.loanApprovalHistory?.historyDetaildata[0]?.lblPayNo;
      }
      $('#detail_modal').modal('show');
    }
    this.spinner.hide();
  }

  async approval_modal(obj:any){
    if(confirm("Are you sure, you want to approve?")){
      console.log("hellllo");
      let data1 ={
        my_url : 'api/Loan/Processing_Approve',
        Id : obj.ID,
        EmpId : obj.PersonalId,
      }
      const res: any = await this.commonService.commonServicePost(data1);
      console.log(res);
      this.toastr.success('Success!', 'Approved successfully.');
      this.getPendingLoanlist();
      
    }

  }

  async reject_modal(obj:any){
    if(confirm("Are you sure, you want to reject?")){
      let data1 ={
        my_url : 'api/Loan/Processing_Reject',
        Id : obj.ID,
        EmpId : obj.PersonalId,
        Remarks:''
      }
      const res: any = await this.commonService.commonServicePost(data1);
      console.log(res);
      this.toastr.success('Success!', 'Rejected successfully.');
      this.getPendingLoanlist();
    }
  }
}
