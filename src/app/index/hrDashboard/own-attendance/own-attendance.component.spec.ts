import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnAttendanceComponent } from './own-attendance.component';

describe('OwnAttendanceComponent', () => {
  let component: OwnAttendanceComponent;
  let fixture: ComponentFixture<OwnAttendanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OwnAttendanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
