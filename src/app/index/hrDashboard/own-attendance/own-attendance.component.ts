import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';

import { ToastrService } from 'ngx-toastr';
import { UrlService } from 'src/app/services/url.service';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { NgxSpinnerService } from 'ngx-spinner';
import * as Moment from 'moment';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';



declare var $:any;

@Component({
  selector: 'app-own-attendance',
  templateUrl: './own-attendance.component.html',
  styleUrls: ['./own-attendance.component.css']
})
export class OwnAttendanceComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective | undefined;
  isDtInitialized = 'no';
  dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  filter_by='1'

  year_arr:any =[]
  year:any='';
  month ='01'
  from_date=''
  to_date=''
  attendance_list:any=[]
  userid:any =''
  selected:any={startDate: Moment, endDate: Moment}

  constructor(private commonService: CommonService,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.userid = localStorage.getItem('userid');

    let current_year = (new Date()).getFullYear();
    this.year = current_year;
    let last_year = current_year-1;
    this.year_arr =[
      current_year,
      last_year
    ]
    console.log(current_year);
    this.dtOptions = {
		  pagingType: 'full_numbers',
		  pageLength: 100,
			dom: 'Bfrtip',
      bFilter:false,
			buttons: [
			'',
			'',
			'',
		  ],
			responsive: true
		};
    this.getAttendance();
  }

  ngAfterViewInit() {
		
	}
  reset(){
    window.location.reload();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

	rerender(): void {
    console.log("dtInstance",this.isDtInitialized)
		if (this.isDtInitialized == 'yes') {
      console.log("dtInstance",this.dtElement)
		  this.dtElement?.dtInstance.then((dtInstance: any) => {
        console.log("dtInstance",dtInstance)
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		  });
		}else {
      this.isDtInitialized = 'yes';
      this.dtTrigger.next();
    }
	}

	initialize_dtOptions(){ 
		this.rerender();
	}

  async getAttendance(){
    this.spinner.show();
    let data = {
      my_url: 'api/att/MHrAttShow',
      headerkey: 'attendance_list',
      empcode: this.userid,
      month: this.month,
      year: this.year,
      fromdate: this.from_date,
      todate: this.to_date,
      page: "self",
      flag: "",
    }
    
    const res: any = await this.commonService.getAttendanceList(data);
    console.log(res);
    this.attendance_list = res.Detail.EntryListtdata;
    if(this.attendance_list.length>0){
      //this.dtTrigger.distroy();
      this.initialize_dtOptions();
      //this.dtTrigger.next();
    }
    this.spinner.hide();
  }

  async filter_month_wise(){
    let isError = false;

    $('.error').addClass('display_none');

    if(!this.month){
      isError = true;
      $('.month_err').removeClass('display_none').html('select month');
    }
    if(!this.year){
      isError = true
      $('.year_err').removeClass('display_none').html('select year');
    }

    if(isError == false){
      this.getAttendance();
    }
  }
  
  async filter_date_wise(){
    let isError = false;
    let date = new Date(this.selected.end);
    if(this.selected.start==undefined || this.selected.end==undefined){
      isError = true;
      $('.from_date_err').removeClass('display_none').html('select date');
    }

    let date2 = `${('0'+ (date.getMonth() + 1)).slice(-2)}/${('0'+date.getDate()).slice(-2)}/${date.getFullYear()}`;
    console.log(date);
    console.log(date2);
    let date3 = new Date(this.selected.start);
    let date4 = `${('0'+ (date3.getMonth() + 1)).slice(-2)}/${('0' +date3.getDate()).slice(-2)}/${date3.getFullYear()}`;
    console.log(date4);
    this.from_date = date4;
    this.to_date = date2;
    console.log(this.selected);

    if(!this.from_date){
      isError=true;
      $('.from_date_err').removeClass('display_none').html('select date');
    }
    if(isError == false){
     
      this.getAttendance();
    }
  }

  validate_date(){
    if(this.selected.start !== undefined || this.selected.end !== undefined){
      $('.from_date_err').addClass('display_none');
    }   
  }

}
