import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common/common.service';
import * as Moment from 'moment';
import { Subject } from 'rxjs';
import { UrlService } from 'src/app/services/url.service';
import { DataTableDirective } from 'angular-datatables';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';
import { ToastrService } from 'ngx-toastr';
declare var $:any

@Component({
  selector: 'app-emp-attendance-mapping',
  templateUrl: './emp-attendance-mapping.component.html',
  styleUrls: ['./emp-attendance-mapping.component.css']
})

export class EmpAttendanceMappingComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective | undefined;
  isDtInitialized = 'no';dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  filter_by='1'
  employeeSearchForm: FormGroup;
  year_arr:any =[]
  year:any='';
  month ='01'
  EffFrom=''
  EffTo=''
  attendance_mapping_list:any=[]
  userid:any =''
  employee_code:any =''
  empcode:any =''
  attendance_type_list:any =[];
  Type=''
  selected:any={startDate: Moment, endDate: Moment(new Date("12/15/25 00:00:00"))}
  employeeList:any =[]
  postedby: any;
  
  constructor(private commonService: CommonService,private spinner: NgxSpinnerService,private fb: FormBuilder,private itServiceDesk: ItServiceDeskService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.userid = localStorage.getItem('userid');

    let current_year = (new Date()).getFullYear();
    this.year = current_year;
    let last_year = current_year-1;
    this.year_arr =[
      current_year,
      last_year
    ]
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 100,
	    dom: 'Bfrtip',
	    buttons: [
        'copy',
        'print',
        'excel',
      ],
	  responsive: true
    };

   this.selected.end = new Date("12/15/49 00:00:00");
    //this.initialize_dtOptions();
    console.log(current_year);
    this.MhrEmpAttMapping_List();
    this.getMhrEmpAttMappingType_List();

    this.employeeSearchForm = this.fb.group({
      searchType: [1],
      searchValue: ['', Validators.required]
    })
   
  }
  ngAfterViewInit() {
		
	}

  initialize_dtOptions(){
    this.rerender()
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

	rerender(): void {
    console.log("dtInstance",this.isDtInitialized)
		if (this.isDtInitialized == 'yes') {
      console.log("dtInstance",this.dtElement)
		  this.dtElement?.dtInstance.then((dtInstance: any) => {
        console.log("dtInstance",dtInstance)
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		  });
		} else {
        this.isDtInitialized = 'yes';
        this.dtTrigger.next();
      }
	}

  async MhrEmpAttMapping_List(){
    let data = {
      my_url: 'api/att/MhrEmpAttMapping_List',
      empcode: this.empcode || this.employee_code,
      flag: "grid",
    }
    this.spinner.show();
    const res: any = await this.commonService.MhrEmpAttMapping_List(data);
    console.log(res);
    
    this.attendance_mapping_list = res.Detail.data;
    if(this.attendance_mapping_list.length>0){
      //this.dtTrigger.distroy();
      this.initialize_dtOptions();
      //this.dtTrigger.next();
    }
    //this.dtTrigger.next();
    this.spinner.hide();
  }

 async filter_month_wise(){
    let isError = false;
    $('.error').addClass('display_none');
    if(!this.employee_code){
      isError = true
      $('.employee_code_err').removeClass('display_none').html('required');
    }

    if(isError == false){
      this.MhrEmpAttMapping_List();
    }
  }

  validate_employee_code(){
    this.empcode =''
    if(!this.employee_code){
      $('.employee_code_err').removeClass('display_none').html('required');
    }else{
      $('.employee_code_err').addClass('display_none');
    }
  }

  validate_empcode(){
    this.employee_code=''
    if(!this.empcode){
      $('.empcode_err').removeClass('display_none').html('required');
    }else if(this.empcode.length !==8){
      $('.empcode_err').removeClass('display_none').html('Emp code must be of 8 digits');
    }else{
      $('.empcode_err').addClass('display_none');
      console.log("func called");
      this.MhrEmpAttMapping_List()
    }
  }

  async getMhrEmpAttMappingType_List(){
    let data = {
      my_url: 'api/att/MhrEmpAttMappingType_List',
    }
    this.spinner.show();
    const res: any = await this.commonService.getMhrEmpAttMappingType_List(data);
    console.log(res);
    this.attendance_type_list = res.Detail.data;
    this.spinner.hide();
  }

  validate_date(event:any){
    console.log(event);
    if(this.selected.start !== undefined || this.selected.end !== undefined){
      $('.from_date_err').addClass('display_none');
    }   
  }

 async save_attendance_mapping(){
    let isError = false;
    if(!this.empcode){
      isError=true
      $('.empcode_err').removeClass('display_none').html('required');
    }else if(this.empcode.length !==8){
      isError = true
      $('.empcode_err').removeClass('display_none').html('Emp code must be of 8 digits');
    }

    if(!this.Type){
      isError=true
      $('.attendance_type_err').removeClass('display_none').html('required');
    }

    if(this.selected.start == undefined || this.selected.end == undefined){
      isError =true
      $('.from_date_err').removeClass('display_none').html('required');
    } 
    let date3 = new Date(this.selected.start);
    this.EffFrom = `${('0'+ (date3.getMonth() + 1)).slice(-2)}/${('0' +date3.getDate()).slice(-2)}/${date3.getFullYear()}`;
    
    let date = new Date(this.selected.end);
    this.EffTo = `${('0'+ (date.getMonth() + 1)).slice(-2)}/${('0'+date.getDate()).slice(-2)}/${date.getFullYear()}`;
    
    if(isError == false){
      let data = {
        my_url: 'api/att/MhrEmpAttMappingSave',
        empcode: this.empcode,
        EffFrom: this.EffFrom,
        EffTo: this.EffTo,
        Type:this.Type
      }
      this.spinner.show();
      const res: any = await this.commonService.MhrEmpAttMappingSave(data);
      console.log(res);
      if(res.status == 401){
        alert(res.Message);
      }else{
        this.toastr.success('Success!', 'Saved successfully.');
        this.empcode='';
        this.Type=''
        this.EffFrom=''
        this.EffTo=''
        this.selected={startDate: Moment, endDate: Moment}
      }
    }
  }

  validate_type(){
    if(this.Type){
      $('.attendance_type_err').addClass('display_none');
    }else{
      $('.attendance_type_err').removeClass('display_none').html('required');
    }
  }

  get searchValue() {
    return this.employeeSearchForm.get('searchValue')
  }

  get searchType() {
    return this.employeeSearchForm.get('searchType')
  }

  async searchEmployee() {
    if (this.searchValue?.value == '') {
      return;
    }
    // console.log(this.employeeSearchForm.value)
    const res: any = await this.itServiceDesk.getEmployeeDetails(this.searchValue?.value, this.searchType?.value)
    this.employeeList = res.Detail.data;
    console.log(this.employeeList)
  }
  
  setEmpCode(val: any) {
    // console.log(val)
    this.empcode =val;
    this.MhrEmpAttMapping_List()
    // $('#empSearchModal').modal('hide')

  }
}
