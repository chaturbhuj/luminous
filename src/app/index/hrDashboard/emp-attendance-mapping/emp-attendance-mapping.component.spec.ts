import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpAttendanceMappingComponent } from './emp-attendance-mapping.component';

describe('EmpAttendanceMappingComponent', () => {
  let component: EmpAttendanceMappingComponent;
  let fixture: ComponentFixture<EmpAttendanceMappingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpAttendanceMappingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpAttendanceMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
