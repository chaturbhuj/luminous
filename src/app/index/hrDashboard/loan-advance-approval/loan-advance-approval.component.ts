import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UrlService } from 'src/app/services/url.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

declare var $: any;
@Component({
  selector: 'app-loan-advance-approval',
  templateUrl: './loan-advance-approval.component.html',
  styleUrls: ['./loan-advance-approval.component.css']
})
export class LoanAdvanceApprovalComponent implements  AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective | undefined;
  isDtInitialized = 'no';dtInstance: DataTables.Api;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  loanApprovalList:any =[];
  loanApprovalHistory:any =[];
  employe_code:any;
  uploadedFile:any;
  amount:any;
  payment_request_no:any;
  reason:any = '';
  emi_type:any = '';
  Id:any = '';
  userid:any = '';
  lblPayNo:any = '';
  reject_reason:any = '';
  p: number = 1;
  img1=''
  constructor(
    private fb: FormBuilder,
    private commonService: CommonService,
    private router: Router,
    private toastr: ToastrService,
    private serverurl: UrlService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.userid = localStorage.getItem('userid');
    this.dtOptions = {
		  pagingType: 'full_numbers',
		  pageLength: 10,
			dom: 'Bfrtip',
			buttons: [
			'copy',
			'print',
			'excel',
		  ],
			responsive: true
		};
    this.gethrLoanlist();
    
  }
  ngAfterViewInit() {
		
	}
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
	rerender(): void {
    console.log("dtInstance",this.isDtInitialized)
		if (this.isDtInitialized == 'yes') {
      console.log("dtInstance",this.dtElement)
		  this.dtElement?.dtInstance.then((dtInstance: any) => {
        console.log("dtInstance",dtInstance)
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		  });
		} else {
        this.isDtInitialized = 'yes';
        this.dtTrigger.next();
      }
	}
	initialize_dtOptions(){ 
		this.rerender();
	}

  async gethrLoanlist() {
    this.spinner.show();
    let data ={
      my_url : 'api/Loan/LoanHR_list',
      headerKey : 'HRCode',
      headervalue : this.userid,
    }
    const res: any = await this.commonService.commonServiceGet(data);
    console.log(res);
    this.loanApprovalList = res.Detail.HRdata;
    if(this.loanApprovalList.length>0){
      //this.dtTrigger.distroy();
      this.initialize_dtOptions();
      //this.dtTrigger.next();
    }
    this.spinner.hide();
  }

  // selectFiles(event: any) {
  //   this.uploadedFile = event.target.files;
  //   console.log(this.uploadedFile);
  // }

  async selectFiles(event: any) {
    var get_file =event.target.files[0];
    await this.getBase64(get_file, (result:any) => {
      console.log(result);
      this.uploadedFile = get_file.name;
      console.log(this.uploadedFile);
      var base64result = result.split(',')[1];
      this.img1 = base64result;
      console.log(this.img1);
    });
  }

  getBase64(file:any, cb:any) {
		return new Promise(resolve => {
			let reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				resolve(cb(reader.result));
			};
			reader.onerror = function (error) {
				//console.log('Error: ', error);
			};
		});
   
	}

  approval_modal(data:any){
    console.log(data);
    this.Id = data['ID'];
    console.log(this.Id);
    this.emi_type = data.emitype;
    this.employe_code = data.PersonalId;
    this.amount = parseFloat(data.Amount);
    this.reason = '';
    this.payment_request_no = '';
    $('.upload_file').val('');
    $('.required').addClass('display_none');
    $('#approval_modal').modal('show');
  }

  reject_modal(data:any){
    console.log(data);
    this.Id = data['ID'];
    console.log(this.Id);
    this.emi_type = data.emitype;
    this.employe_code = data.PersonalId;
    this.amount = parseFloat(data.Amount);
    this.reject_reason = '';
    $('.required').addClass('display_none');
    $('#reject_modal').modal('show');
  }

  async approve_loan(){
    $('.required').addClass('display_none');
    let error = false;
    if(!this.amount){
      $('.amountError').removeClass('display_none');
      error=true;
    }
    if(!this.emi_type){
      $('.emitypeError').removeClass('display_none');
      error=true;
    }
    if(!this.reason){
      $('.reasonError').removeClass('display_none');
      error=true;
    }
    if(!this.payment_request_no){
      $('.prnError').removeClass('display_none');
      error=true;
    }
    // if(!this.uploadedFile){
    //   $('.uploadError').removeClass('display_none');
    //   error=true;
    // }
    if(error==true){
      return;
    }
    console.log(this.uploadedFile);
    
    console.log(this.Id);
    this.spinner.show();
    let data ={
      my_url : 'api/Loan/HR_Approve',
      EmpId : this.employe_code,
      Id : this.Id,
      Remarks : this.reason,
      file : this.uploadedFile,//this.uploadedFile,
      Amount : this.amount,
      PayRequestNo : this.payment_request_no,
      Emitype : this.emi_type,
      AppEmpCode : this.userid,
      img1 : this.img1,
      mode : '1',
    }
    const res: any = await this.commonService.commonServicePost(data);
    console.log(res);
    this.spinner.hide();
    if(res.status == 200){
      this.employe_code = '';
      this.amount = '';
      this.Id = '';
      this.reason = '';
      this.payment_request_no = '';
      this.emi_type = '';
      $('#approval_modal').modal('hide');
      this.gethrLoanlist();
      this.toastr.success('Success!', 'Approved successfully.');
    }
  }

  async reject_loan(){
    $('.required').addClass('display_none');
    let error = false;
    if(!this.reject_reason){
      $('.reasonError').removeClass('display_none');
      error=true;
    }
    if(error==true){
      return;
    }

    this.spinner.show();
    let data ={
      my_url : 'api/Loan/HR_Reject',
      EmpId : this.employe_code,
      Id : this.Id,
      Remarks : this.reject_reason,
      file : '',
      Amount : this.amount,
      PayRequestNo : '',
      Emitype : this.emi_type,
      AppEmpCode : this.userid,
      img1 : '',
      mode : '2',
    }
    const res: any = await this.commonService.commonServicePost(data);
    console.log(res);
    this.spinner.hide();
    if(res.status == 200){
      $('#reject_modal').modal('hide');
      this.gethrLoanlist();
      this.toastr.success('Success!', 'Rejected successfully.');
    }
  }

  async open_detail_page(data:any){
    //console.log(data);
    //window.open(this.serverurl.Baseurl+'loanAdvanceHistory?RequestId='+data.ID+'&empcode='+data.PersonalId, "_blank");
    this.spinner.show();
    let data1 ={
      my_url : 'api/Loan/LoanHistory',
      headerKey : 'RequestHistory',
      headervalue : data.ID,
      headervalue1 : data.PersonalId,
    }
    const res: any = await this.commonService.commonServiceGet(data1);
    console.log(res);
    if(res.Detail){
      this.loanApprovalHistory = res.Detail;
      if(this.loanApprovalHistory?.historyDetaildata?.length){
        this.lblPayNo = this.loanApprovalHistory?.historyDetaildata[0]?.lblPayNo;
      }
      $('#detail_modal').modal('show');
    }
    this.spinner.hide();
  }
  
  download_file(data:any){
    console.log(data);
  }
  
}
