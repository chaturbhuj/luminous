import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanAdvanceApprovalComponent } from './loan-advance-approval.component';

describe('LoanAdvanceApprovalComponent', () => {
  let component: LoanAdvanceApprovalComponent;
  let fixture: ComponentFixture<LoanAdvanceApprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanAdvanceApprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanAdvanceApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
