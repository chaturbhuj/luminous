import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HrDashboardFooterComponent } from './hr-dashboard-footer.component';

describe('HrDashboardFooterComponent', () => {
  let component: HrDashboardFooterComponent;
  let fixture: ComponentFixture<HrDashboardFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HrDashboardFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HrDashboardFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
