import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';
import { Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common/common.service';
import { ToastrService } from 'ngx-toastr';
declare var $:any

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  ticketList: any;
  tempTicketList: any;
  resultsLength: any;
  checkTech: any = localStorage.getItem('technician');
  p: number = 1;
  collection: any[] = [];
  filterVal: any;
  uploaded_file_list:any =[]
  

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '6rem',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
   
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],

    toolbarPosition: 'top',

    toolbarHiddenButtons: [
      [
        'strikeThrough',
        'subscript',
        'superscript',
        'indent',
        'outdent',
        'insertUnorderedList',
        'insertOrderedList',
        'heading',
      ],
      [
        'backgroundColor',
        'customClasses',
        'unlink',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
      ],
    ],
  };

  attachedFiles: any;
  ticketArr: any = '';
  ticketReplyArr: any = [];
  postId: any;
  technicianControllerList: any;
  uploaded_file =''
  replydesc=''

  constructor(
    private fb: FormBuilder,
    private itServiceDesk: ItServiceDeskService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private commonService: CommonService,
    private toastr: ToastrService
  ) {}
  showDetailFlag: boolean = false;

  applyFilter() {
    this.ticketList = this.tempTicketList.filter((el: any) => {
      return Object.values(el).includes(this.filterVal);
    });
  }
  
  ngOnInit(): void {
    this.getUserTicketList();
  }

  async getUserTicketList() {
    this.spinner.show();
    const res: any = await this.itServiceDesk.getUserTicketList();
    console.log(res);
    this.ticketList = res?.Detail?.data.reverse();
    this.tempTicketList = this.ticketList;
    this.collection = this.ticketList;
    this.spinner.hide();
  }

  // async getTechnicianTicketList() {
  //   this.spinner.show();
  //   const res: any = await this.itServiceDesk.getTechnicianTicketList();
  //   console.log(res);
  //   this.ticketList = res.Detail.data;
  //   this.tempTicketList = this.ticketList;
  //   this.collection = this.ticketList;
  //   this.spinner.hide();
  // }

  showDetail(val: any) {
    console.log(val);
    this.postId = val.postid;
    this.getTicketDetails();
    this.getTicketReplyDetails();
    this.showDetailFlag = true;
  }

  async getTicketDetails() {
    this.spinner.show();
    const res: any = await this.itServiceDesk.getTicketDetails(this.postId);
    this.ticketArr = res.Detail.data[0];
    this.spinner.hide();
    console.log(this.ticketArr);
  }

  async getTicketReplyDetails() {
    this.spinner.show();
    const res: any = await this.itServiceDesk.getTicketReplyDetails(
      this.postId
    );
    this.ticketReplyArr = res.Detail.data;
    this.spinner.hide();
    console.log(this.ticketReplyArr);
  }

  async selectFiles(event: any) {
    this.attachedFiles = event.target.files[0];
    console.log(this.attachedFiles);
    // var image = this.getBase64(this.attachedFiles);
    // console.log(image);
    await this.getBase64(this.attachedFiles, (result:any) => {
      console.log(result);
      this.upload_file(result,this.attachedFiles.name)
    });
  }

  getBase64(file:any, cb:any) {
		return new Promise(resolve => {
			let reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				resolve(cb(reader.result));
			};
			reader.onerror = function (error) {
				//console.log('Error: ', error);
			};
		});
	}

  async upload_file(file:any,name:any){
    var base64result = file.split(',')[1];
    var extension = name.split('.')[1];
    console.log(file);
    console.log(name);
    console.log(extension);
    
    let data = {
      serverurl: 'https://lumapi.luminousindia.com/ITServiceDeskWebapi/',
      my_url : 'api/LogTicket/Addtachment',
      filename:name,
      file:base64result,
      extention:'.'+extension
    };

    let data1 = { "data": [data]}
    
    console.log(data1);

    const res: any = await this.commonService.commonUploadImage(data1);
    console.log(res);
    this.attachedFiles='';
    this.uploaded_file= name;
    console.log(this.uploaded_file);
    $('.uploaded_file').val('');
    let data2 = {
      ticketid: '0',
    };
    const response: any = await this.itServiceDesk.GetAddtachment(data2);
    console.log(response);
    this.uploaded_file_list = response.Detail.data;
    
  
  }

  back() {
    this.showDetailFlag = false;
  }

  async selectTechnicianControllerlist(reqType: any) {
    console.log(reqType);
    const res: any = await this.itServiceDesk.selectTechnicianControllerlist(
      reqType
    );
    this.technicianControllerList = res?.Detail.data;
    console.log(this.technicianControllerList);
  }

  checkType(val: any) {
    let check = this.technicianControllerList?.find(
      (el: any) => el.Controller.trim() == val
    );
    if (check) {
      return true;
    } else {
      return false;
    }
  }

  async submit_form(){
    if(!this.replydesc){
      $('.reply_desc_err').removeClass('display_none');
    }else{
      let userid =localStorage.getItem('userid');
      console.log(userid)
      let data = {
        ticketid:this.postId,
        replydesc:this.replydesc,
        ass: userid
      };  
      console.log(data);
  
      const res: any = await this.itServiceDesk.PostReplyUser(data);
      console.log(res);
      this.replydesc ='';
      this.uploaded_file ='';
      $('.uplaoded_file').val('');
      this.toastr.success('Success!', 'Sbumitted successfully.');
      
    }
  }
}
