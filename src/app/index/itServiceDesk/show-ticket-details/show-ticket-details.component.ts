import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { CommonService } from 'src/app/services/common/common.service';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';

@Component({
  selector: 'app-show-ticket-details',
  templateUrl: './show-ticket-details.component.html',
  styleUrls: ['./show-ticket-details.component.css'],
})
export class ShowTicketDetailsComponent implements OnInit {
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '6rem',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],

    toolbarPosition: 'top',

    toolbarHiddenButtons: [
      [
        'strikeThrough',
        'subscript',
        'superscript',
        'indent',
        'outdent',
        'insertUnorderedList',
        'insertOrderedList',
        'heading',
      ],
      [
        'backgroundColor',
        'customClasses',
        'unlink',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
      ],
    ],
  };

  constructor(
    private itServiceDesk: ItServiceDeskService,
    private route: ActivatedRoute,
    private commonService: CommonService
    
  ) {}
  attachedFiles: any;
  ticketArr: any = '';
  ticketReplyArr: any = [];
  postId = this.route.snapshot.params['id'];
  uploaded_file=''
  uploaded_file_list:any =[]

  ngOnInit(): void {
    this.getTicketDetails();
    this.getTicketReplyDetails();
  }

  async getTicketDetails() {
    const res: any = await this.itServiceDesk.getTicketDetails(this.postId);
    this.ticketArr = res.Detail.data[0];
    console.log(this.ticketArr);
  }

  async getTicketReplyDetails() {
    const res: any = await this.itServiceDesk.getTicketReplyDetails(
      this.postId
    );
    this.ticketReplyArr = res.Detail.data;
    console.log(this.ticketReplyArr);
  }
  async selectFiles(event: any) {
    this.attachedFiles = event.target.files[0];
    console.log(this.attachedFiles);
    // var image = this.getBase64(this.attachedFiles);
    // console.log(image);
    await this.getBase64(this.attachedFiles, (result:any) => {
      console.log(result);
      this.upload_file(result,this.attachedFiles.name)
    });
  }

  getBase64(file:any, cb:any) {
		return new Promise(resolve => {
			let reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				resolve(cb(reader.result));
			};
			reader.onerror = function (error) {
				//console.log('Error: ', error);
			};
		});
	}

  async upload_file(file:any,name:any){
    var base64result = file.split(',')[1];
    var extension = name.split('.')[1];
    console.log(file);
    console.log(name);
    let data = {
      serverurl: 'https://lumapi.luminousindia.com/ITServiceDeskWebapi/',
      my_url : 'api/LogTicket/Addtachment',
      filename:name,
      file:base64result,
      extention:'.'+extension
    };

    let data1 = { "data": [data]}
    console.log(data1);

    const res: any = await this.commonService.commonUploadImage(data1);
    console.log(res);
    this.attachedFiles='';
    this.uploaded_file= name;
    $('.uploaded_file').val('');

    let data2 = {
      ticketid: '0',
    };
    console.log(data1);

    const response: any = await this.itServiceDesk.GetAddtachment(data2);
    console.log(response);
    this.uploaded_file_list = response.Detail.data;
    
  }
}
