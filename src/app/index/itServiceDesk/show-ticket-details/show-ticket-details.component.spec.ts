import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTicketDetailsComponent } from './show-ticket-details.component';

describe('ShowTicketDetailsComponent', () => {
  let component: ShowTicketDetailsComponent;
  let fixture: ComponentFixture<ShowTicketDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowTicketDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTicketDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
