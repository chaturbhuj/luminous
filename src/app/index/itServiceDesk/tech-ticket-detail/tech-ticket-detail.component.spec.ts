import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTicketDetailComponent } from './tech-ticket-detail.component';

describe('TechTicketDetailComponent', () => {
  let component: TechTicketDetailComponent;
  let fixture: ComponentFixture<TechTicketDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechTicketDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTicketDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
