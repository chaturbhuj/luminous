import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/services/common/common.service';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';

declare var $:any;
@Component({
  selector: 'app-tech-ticket-detail',
  templateUrl: './tech-ticket-detail.component.html',
  styleUrls: ['./tech-ticket-detail.component.css'],
})
export class TechTicketDetailComponent implements OnInit, OnChanges {
  constructor(
    private fb: FormBuilder,
    private itServiceDesk: ItServiceDeskService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private commonService: CommonService,
    private toastr: ToastrService
  ) {}

  priority:any=''
  ticketList: any;
  tempTicketList: any;
  resultsLength: any;
  checkTech: any = localStorage.getItem('technician');
  p: number = 1;
  collection: any[] = [];
  filterVal: any;
  filterForm: FormGroup;
  statusArr: any;
  itemArr: any;
  popupWindow=''
  @Input() tabVal: any = '';
  uploaded_file ='';
  uploaded_file_list:any =[]
  ITEMListTechTicket:any =[]
  AssignToList:any =[]
  CheckMailRadio:any =[]
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '6rem',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],

    toolbarPosition: 'top',

    toolbarHiddenButtons: [
      [
        'strikeThrough',
        'subscript',
        'superscript',
        'indent',
        'outdent',
        'insertUnorderedList',
        'insertOrderedList',
        'heading',
      ],
      [
        'backgroundColor',
        'customClasses',
        'unlink',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
      ],
    ],
  };

  attachedFiles: any;
  ticketArr: any = '';
  ticketReplyArr: any = [];
  postId: any;
  technicianControllerList: any;
  showDetailFlag: boolean = false;
  reply_text=''
  status=''
  search_emp_type=''
  assignto=''
  flexRadioDefault='1'
  userid:any=''
  implementation=''
  test_result=''
  rollback_plan=''
  downtime_window=''
  priority_arr:any =[]
  new_priority=''
  transport=''
  mail_approval_assignto=''

  ngOnInit() {
    console.log(this.tabVal);
    
    this.userid = localStorage.getItem('userid');
    this.assignto = this.userid;
    this.getStatusList();
    this.getEmpItemList();
    this.getITEMListTechTicket();
    this.getCheckMailRadio()
    this.filterForm = this.fb.group({
      status: ['',],
      assgin: ['',],
      logby: [''],
      ticketNo: [''],
      Stdate: [''],
      Enddate: [''],
      item: ['0'],
      days: [180],
    });
    //this.delete_uploaded_file()
    this.status = $('.status').find(":selected").text();
    this.priority_arr =[
      {
        id: 1,
        val:'P1'
      },
      {
        id: 2,
        val:'P2'
      },
      {
        id: 3,
        val:'P3'
      },
      {
        id: 4,
        val:'P4'
      },
      {
        id: 5,
        val:'P5'
      }
    ];

  
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.tabVal)
    if (this.statusArr?.length > 0) {
      this.getTechnicianTicketList();
    }
    
  }

  check_transport(){
    if(this.transport){
      this.assignto= '10903207'
    }else{
      this.assignto = this.userid
    }
  }

  async delete_uploaded_file(){
    let data = {
      serverurl: 'https://lumapi.luminousindia.com/ITServiceDeskWebapi/',
      my_url : 'api/LogTicket/DeleteAddtachment_pageload',
 
    };

    let data1 = { "data": [data]}
    console.log(data1);

    const res: any = await this.commonService.commonServicePost(data1);
  }

  applyFilter() {
    this.ticketList = this.tempTicketList.filter((el: any) => {
      return Object.values(el).includes(this.filterVal);
    });
  }

  showDetail(val: any) {
    console.log(val);
    this.postId = val.postid;
    this.getTicketDetails();
    this.getTicketReplyDetails();
    this.showDetailFlag = true;
    let data2 = {
      ticketid: this.postId,
    };
    this.getAttachmentList(data2);
    console.log(this.uploaded_file_list);
  }

  async selectFiles(event: any) {
    this.attachedFiles = event.target.files[0];
    console.log(this.attachedFiles);
    // var image = this.getBase64(this.attachedFiles);
    // console.log(image);
    await this.getBase64(this.attachedFiles, (result:any) => {
      console.log(result);
      this.upload_file(result,this.attachedFiles.name)
    });
  }

  getBase64(file:any, cb:any) {
		return new Promise(resolve => {
			let reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				resolve(cb(reader.result));
			};
			reader.onerror = function (error) {
				//console.log('Error: ', error);
			};
		});
	}

  async upload_file(file:any,name:any){
    var base64result = file.split(',')[1];
    var extension = name.split('.')[1];
    console.log(file);
    console.log(name);
    let data = {
      serverurl: 'https://lumapi.luminousindia.com/ITServiceDeskWebapi/',
      my_url : 'api/LogTicket/Addtachment',
      filename:name,
      file:base64result,
      extention:'.'+extension
    };

    let data1 = { "data": [data]}
    console.log(data1);

    const res: any = await this.commonService.commonUploadImage(data1);
    console.log(res);
    this.attachedFiles='';
    this.uploaded_file= name;
    $('.uploaded_file').val('');

    let data2 = {
      ticketid: this.ticketArr.postid,
    };
    console.log(data1);
    this.getAttachmentList(data2)
   
    
  }
  async getAttachmentList(data2:any){
    const response: any = await this.itServiceDesk.GetAddtachment(data2);
    console.log(response);
    this.uploaded_file_list = response.Detail.data;
  }

  back() {
    this.showDetailFlag = false;
  }

  async getTechnicianTicketList() {
    this.showDetailFlag = false;
    console.log(this.tabVal);
    if (this.tabVal == 'Unassigned') {
      this.filterForm?.get('item')?.patchValue('0');
      this.filterForm?.get('status')?.patchValue('1');
      this.filterForm?.get('assgin')?.patchValue(0);
    } else {
      console.log(this.tabVal);
      this.filterForm
        ?.get('status')
        ?.patchValue(
          this.statusArr.find((el: any) => el.statusdesc == this.tabVal)
            .statusid
        );
      this.filterForm?.get('assgin')?.patchValue(1);
    }

    console.log(this.filterForm.value);

    try {
      this.spinner.show();
      const res: any = await this.itServiceDesk.getTechnicianTicketList(
        this.filterForm.value
      );
      console.log(res);
      if(res.status == 200){
        this.ticketList = res.Detail.data;
        this.tempTicketList = this.ticketList;
        this.collection = this.ticketList;

      }else if(res.status == 401){
        this.ticketList=[]
        this.tempTicketList=[]
        this.collection=[]
        alert(res.Message)
      }
  
      this.spinner.hide();
    } catch (err: any) {
      this.spinner.hide();
      console.log(err);
      alert(err.error.Message);
    }
  }


  async selectTechnicianControllerlist(reqType: any) {
    console.log(reqType);
    const res: any = await this.itServiceDesk.selectTechnicianControllerlist(reqType);
    this.technicianControllerList = res?.Detail?.data;
    console.log(this.technicianControllerList);
  }

  async getTicketDetails() {
    this.spinner.show();
    const res: any = await this.itServiceDesk.getTicketDetails(this.postId);
    this.ticketArr = res.Detail.data[0];
    this.selectTechnicianControllerlist(this.ticketArr.ReqTypeid)
    this.spinner.hide();
    console.log(this.ticketArr);
     
    let get_status =this.statusArr.find((item: any)=>item.statusid == this.ticketArr.status);
    this.status = get_status.statusid;
   console.log(this.status);
  }

  async getTicketReplyDetails() {
    this.spinner.show();
    const res: any = await this.itServiceDesk.getTicketReplyDetails(
      this.postId
    );
    this.ticketReplyArr = res.Detail.data;
    this.spinner.hide();
    console.log(this.ticketReplyArr);
  }

  async getEmpItemList() {
    const res: any = await this.itServiceDesk.getEmpItemList();
    // console.log(res);
    this.itemArr = res.Detail.data;
  }

  async saveSelectionDay() {
    const res: any = await this.itServiceDesk.saveSelectionDay(
      this.filterForm?.get('days')?.value
    );
    // console.log(res);
    if (res.Message == 'success') {
      alert('Successfully Saved');
    } else {
      alert(res.Message);
    }
  }

  refresh() {
    window.location.reload();
  }

  async pickupPopup(postid:any,priority:any){
   // this.popupWindow ='Service'
    this.postId=postid
    this.priority = this.get_priroity(priority);
    
    console.log(this.priority);
    
   const res:any = await this.itServiceDesk.getPickupPopup(postid)
   console.log(res);
   this.popupWindow = res.Popupwindow;
    $('#Priority').modal('show');
  }

  closePriorityModal(){
    $('#Priority').modal('hide');
  }

  get_priroity(priority:any){
    let p =1;
    if(priority == 'P1') p= 1;
    if(priority == 'P2') p= 2;
    if(priority == 'P3') p= 3;
    if(priority == 'P4') p= 4;
    if(priority == 'P5') p= 5;
    return p;
  }

  async pickupSave(){
    let data ={
      postid: this.postId,
      Priority:this.priority,
      impactofchange:0,
      UrgencyCategory:0,
      ImpactCategory:0
    }
    console.log(data);
    
    const res:any = await this.itServiceDesk.pickupSave(data)
    console.log(res);
    this.toastr.success('Success!', 'Saved successfully.');
    this.closePriorityModal();
    setTimeout(() => {
      window.location.reload();
    }, 1000);
    
  }

  async ReturnRequestbyAdmin(postid:any){
    let isError = false;
    if(!this.reply_text){
      isError=true;
      $('.reply_text_err').removeClass('display_none').html("required");
    }

    if(isError == false){
      let data ={
        postid: postid,
        reply: this.reply_text
      }
      console.log(data);
      
      const res:any = await this.itServiceDesk.ReturnRequestbyAdmin(data)
      console.log(res);
      this.toastr.success('Success!', 'Saved successfully.');
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }
   
  }

  checkType(val: any) {
    let check = this.technicianControllerList?.find(
      (el: any) => el.Controller.trim() == val
    );
    if (check) {
      return true;
    } else {
      return false;
    }
  }

  async getStatusList() {
    this.spinner.show();
    const res: any = await this.itServiceDesk.getStatusList();
    this.statusArr = res.Detail.data;
    this.spinner.hide();
    console.log(this.statusArr);
    this.getTechnicianTicketList();
    console.log(this.ticketArr);
   
   
  }


  async getITEMListTechTicket(){
    this.spinner.show();
    const res: any = await this.itServiceDesk.getITEMListTechTicket();
    this.ITEMListTechTicket = res.Detail.data;
    this.spinner.hide();
    console.log(this.ITEMListTechTicket);
  }

  async GetAssignToList(event:any){
    if(this.search_emp_type.length){
      console.log("GetAssignToList called");
      console.log(event.target.value);
    
      this.spinner.show();
      const res: any = await this.itServiceDesk.GetAssignToList(
      {
        postId: this.postId,
        techitem:event.target.value
      });

     this.AssignToList = res.Detail.data;
     this.spinner.hide();
     console.log(this.AssignToList);
    }else{
      this.AssignToList= []
      this.assignto=''
    }
    
  }

  async getCheckMailRadio(){
    this.spinner.show();
    const res: any = await this.itServiceDesk.CheckMailRadio();
    this.CheckMailRadio = res.Message;
    this.spinner.hide();
    console.log(this.CheckMailRadio);
  }


  async submit_ticket(){
   
    let err_msg = 'required field';
    let isError = false;
    $('.error').addClass('display_none');
    if(!this.reply_text){
      isError=true;
      $('.reply_text_err').removeClass('display_none').html(err_msg);
    }
 
    if(!this.status){
      isError=true;
      $('.status_err').removeClass('display_none').html(err_msg);
    }
    // if(!this.search_emp_type){
    //   isError=true;
    //   $('.search_emp_type_err').removeClass('display_none').html(err_msg);
    // }
    // if(!this.assignto){
    //   isError=true;
    //   $('.assignto_err').removeClass('display_none').html(err_msg);
    // }
    if(!this.flexRadioDefault){
      isError=true;
      $('.flexRadioDefault_err').removeClass('display_none').html(err_msg);
    }

    if(this.ticketArr.ReqTypeid==3){
      if(!this.implementation){
        isError=true;
        $('.implementation_err').removeClass('display_none').html(err_msg);
      }
      if(!this.test_result){
        isError=true;
        $('.test_result_err').removeClass('display_none').html(err_msg);
      }
      if(!this.downtime_window){
        isError=true;
        $('.downtime_window_err').removeClass('display_none').html(err_msg);
      }
      if(!this.rollback_plan){
        isError=true;
        $('.rollback_plan_err').removeClass('display_none').html(err_msg);
      }
    }
   
    console.log(this.assignto);
    
    if(isError == false){
      if(this.ticketArr.SdlcDays >5){
        let data ={
          status: this.status,
          assginto:this.assignto,
          changetype:this.ticketArr.TChange,
          postid: this.postId,
        }
        const res: any = await this.itServiceDesk.CheckUTSDLC(data);
        console.log(res);
        if(res.status == 200){
          
        }
      }
      this.CheckChilticket();
     // this.CheckAssginusermail();
     // this.savereply();
    } 
  
  }

  async CheckChilticket(){
    const res: any = await this.itServiceDesk.CheckChilticket(this.postId);
    console.log(res);
    if(res.status==200){
      this.CheckAssginusermail();
    }else{
      console.log(res.Message);
    }
  }

  async CheckAssginusermail(){
    let data={
      postid: this.postId,
      assginto: this.assignto
    }
    console.log(data);
    
    const res: any = await this.itServiceDesk.CheckAssginusermail(data);
    console.log(res);
    if(res.status==200){
      this.savereply();
    }else{
      console.log(res.Message);
    }
  }

  async savereply(){
    let data ={
      "data":[{
        postid:"509711",
        replydesc:this.reply_text,
        reqytype:this.ticketArr.ReqTypeid,
        ImplementationPlan:this.implementation,
        df:"",
        Testresul:this.test_result,
        Expecteddate:"",
        DownWin:this.downtime_window,
        RollPlan:this.rollback_plan,
        Resolutionactivities:"",
        TestresultsIncidinet:"",
        RootCause:"",
        DetailedRootCause:"",
        Associaterecordidentifiers:"",
        ImpacttoLuminous:"",
        replyby:this.userid,
        flag:"0",
        DevelopmentStartDate:"",
        efortday:"",
        assginto:this.assignto,
        status:this.status,
        mailcheck:"1",
        checktransport:"0"
      }]
    }

    const res: any = await this.itServiceDesk.savereply(data);
    console.log(res);
    this.toastr.success('Success!', 'Reply saved successfully.');
    window.location.reload();
  }

  validate_input(input_field:any,err_class:any){
    if($('.'+input_field).val() !== ''){
        $('.'+err_class).addClass('display_none');
    }else{
      $('.'+err_class).removeClass('display_none').html('required field');
    }
  }
}
