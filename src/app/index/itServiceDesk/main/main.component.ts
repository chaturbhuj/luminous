import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private itService:ItServiceDeskService, private router:Router) { }
  // mode = new FormControl('over');
  tabVal="home";
  checkTech:any = localStorage.getItem('technician');
  totalEmployeeTickets:any = '';
  ngOnInit(): void {
    if(this.checkTech == 'true'){
      this.getTotalEmployeeTickets();
    }
  }

  switchTab(val:any){
    console.log(val);
    
    this.tabVal = val;
  }

  logout(){
    localStorage.clear();
    this.router.navigateByUrl('login');
  }


  async getTotalEmployeeTickets(){
    try {
      const res:any = await this.itService.getTotalEmployeeTickets();
      this.totalEmployeeTickets = res.Detail.data[0];
      console.log(this.totalEmployeeTickets)
    } catch (err) {
      console.log(err)
    }
  }

  switchToTechnicianTab(val:any){
    console.log(val);
    this.router.navigateByUrl(`itServiceDesk/techTicketDetails/${val}`)
  }

 
}
