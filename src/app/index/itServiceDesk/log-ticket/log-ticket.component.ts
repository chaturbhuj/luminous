import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { CommonService } from 'src/app/services/common/common.service';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';

declare var $:any;
@Component({
  selector: 'app-log-ticket',
  templateUrl: './log-ticket.component.html',
  styleUrls: ['./log-ticket.component.css']
})
export class LogTicketComponent implements OnInit {

  constructor(private itServiceDesk: ItServiceDeskService, private fb: FormBuilder,private commonService: CommonService ) { }
  subCategoryList: any;
  itemList: any;
  locationList: any;
  changeTypeList: any;
  changeDurationList: any;
  employeeSearchForm: FormGroup;
  mainForm: FormGroup;
  employeeList: any;
  requestTypeList: any;
  formSetterList: any;
  incidentFlag: boolean = true;
  exceptionFlag: boolean = true;
  priorityMasterList: any;
  currentPopup:String = '';
  subcat:any = '';
  attachedFiles:any;
  uploaded_file:any=[]
  result:any
  uploaded_file_list:any =[]
  userid = localStorage.getItem('userid');
  checkTech: any = localStorage.getItem('technician');
  p: number = 1;

  ngOnInit(): void {

    this.employeeSearchForm = this.fb.group({
      searchType: [1],
      searchValue: ['', Validators.required]
    })

    this.mainForm = this.fb.group({
      // empcode: [this.userid, Validators.required],
      // typeOfRequest: ['', Validators.required],
      postname: [''],
      postedby: [this.userid, Validators.required],
      cat: '1',
      subcat: ['', Validators.required],
      postdesc: [''],
      item: [''],
      site: [''],
      userid: [this.userid],
      projectid: '0',
      flag: '',
      ReqType: ['', Validators.required],
      AAName: [''],
      RChange: [''],
      IChange: '0',
      TChange: '0',
      TypeIncident: '',
      DateIncident: [''],
      ImpactIncident: [''],
      UrgencyIncident: [''],
      DTIncident: [''],
      LIncident: [''],
      SrvP: [''],
      Changedur: [''],
      tempday: '',
      Just: [''],
      VPeriod: '',
      Risk: [''],
      days: ['']
    })

    console.log(this.mainForm);
    console.log(this.mainForm.controls.item.value);
     ;
    
    this.getCategory()
    this.getSubCategory()
    this.getChangeTypeList()
    this.getChangedurationList()
    this.getRequestypeList()
    this.getPriorityMasterList()

    this.ReqType?.valueChanges.subscribe(() => { this.formSetter() });

    //this.page();

  }

  async page(){
    let data2 = {
      ticketid: '0',
    };
    //console.log(data1);

    const response: any = await this.itServiceDesk.GetAddtachment(data2);
    console.log(response);
    this.uploaded_file_list = response.Detail.data;
  }

  get postedby() {
    return this.mainForm.get('postedby')
  }

  get ReqType() {
    return this.mainForm.get('ReqType')
  }


  get searchValue() {
    return this.employeeSearchForm.get('searchValue')
  }

  get searchType() {
    return this.employeeSearchForm.get('searchType')
  }

  async searchEmployee() {
    if (this.searchValue?.value == '') {
      return;
    }
    // console.log(this.employeeSearchForm.value)
    const res: any = await this.itServiceDesk.getEmployeeDetails(this.searchValue?.value, this.searchType?.value)
    this.employeeList = res.Detail.data;
    console.log(this.employeeList)
  }

  setEmpCode(val: any) {
    // console.log(val)
    this.postedby?.setValue(val);
    // $('#empSearchModal').modal('hide')

  }

  formSetter() {
    this.formSetterList = this.requestTypeList.filter((el: any) => el.Req_Code == this.ReqType?.value);

    console.log(this.formSetterList)
    this.currentPopup = '';
    this.getItemList()
  }

  async getSubCategory() {
    const res: any = await this.itServiceDesk.getSubCategory()
    this.subCategoryList = res.Detail.data;
    console.log(this.subCategoryList)

  }

  async getCategory() {
    const res: any = await this.itServiceDesk.getCategory()
    console.log(res)
  }

  async getPriorityMasterList() {

    const res: any = await this.itServiceDesk.getPriorityMasterList()
    this.priorityMasterList= res.Detail.data;
    console.log(res)

  }
  

  async getRequestypeList() {
    const res: any = await this.itServiceDesk.getRequestypeList()
    this.requestTypeList = res.Detail.data;
  }

  setSubCat(val?: any){
    this.subcat = val.target.value;
    this.ReqType?.setValue('')
    this.itemList = [];
    this.currentPopup = '';
  }

  async getItemList() {
    this.currentPopup = '';
    console.log(this.subcat)
    try {
      const res: any = await this.itServiceDesk.checkSucessfactorlogticket(this.subcat)
      if (res.status == 401) {
        alert(res.Message)
        return;
      }
      this.checkTypeofRequesSubCatwise(this.subcat);
      const res2: any = await this.itServiceDesk.getItemList(this.subcat, this.ReqType?.value)
      this.itemList = res2.Detail.data;
    } catch (error) {
      console.log(error)
    }
    // console.log(res)
  }

  async checkTypeofRequesSubCatwise(val: any) {
    const res: any = await this.itServiceDesk.checkTypeofRequesSubCatwise(val)
    if (res.Detail == "YESYES") {
      this.incidentFlag = true
      this.exceptionFlag = true
    }
    else if (res.Detail == "YESNO") {
      this.incidentFlag = true
      this.exceptionFlag = false
    }
    else if (res.Detail == "NOYES") {
      this.incidentFlag = false
      this.exceptionFlag = true
    }
    else if (res.Detail == "NONO") {
      this.incidentFlag = false
      this.exceptionFlag = false
    }
  }
  
  async getLocationList(val: any) {
    console.log(this.itemList);
    console.log(this.mainForm.controls.item.value);
    
    let itemCode =this.mainForm.controls.item.value;
   // this.currentPopup = this.itemList.itemCode.popup.replace(/ /g,'')
    console.log(this.currentPopup)
    const res: any = await this.itServiceDesk.getLocationList(itemCode)

    console.log(res)
    this.locationList = res.Detail.data;

  }


  async getChangeTypeList() {
    const res: any = await this.itServiceDesk.getChangeTypeList()
    // console.log("change type",res)

    this.changeTypeList = res.Detail.data;

  }

  async getChangedurationList() {
    const res: any = await this.itServiceDesk.getChangedurationList()
    // console.log("change duration",res)
    console.log(res);
    this.changeDurationList = res.Detail.data;

  }


  async logTicket(){
    this.mainForm.markAllAsTouched();
    console.log(this.mainForm.value);
    if (this.mainForm.status == 'INVALID') {
      return;
    }
    const res:any = await this.itServiceDesk.postTicket(this.mainForm.value)
    console.log(res);

    if(res.status == 200){
      alert(`Request Submitted successfully. Your Ticked Id is ${res.Ticketid}`)
      window.location.reload()
    }

  }

  async addAttchment(){

  }

  async selectFiles(event: any) {
    this.attachedFiles = event.target.files[0];
    console.log(this.attachedFiles);
    // var image = this.getBase64(this.attachedFiles);
    // console.log(image);
    await this.getBase64(this.attachedFiles, (result:any) => {
      console.log(result);
      this.upload_file(result,this.attachedFiles.name)
    });
  }

  getBase64(file:any, cb:any) {
		return new Promise(resolve => {
			let reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				resolve(cb(reader.result));
			};
			reader.onerror = function (error) {
				//console.log('Error: ', error);
			};
		});
	}

  async upload_file(file:any,name:any){
    var base64result = file.split(',')[1];
    var extension = name.split('.')[1];
    console.log(file);
    console.log(name);
    let data = {
      serverurl: 'https://lumapi.luminousindia.com/ITServiceDeskWebapi/',
      my_url : 'api/LogTicket/Addtachment',
      filename:name,
      file:base64result,
      extention:'.'+extension
    };

    let data1 = { "data": [data]}
    console.log(data1);

    const res: any = await this.commonService.commonUploadImage(data1);
    console.log(res);
    this.attachedFiles='';
    this.uploaded_file= name;
    $('.uploaded_file').val('');

    let data2 = {
      ticketid: '0',
    };
    console.log(data1);

    const response: any = await this.itServiceDesk.GetAddtachment(data2);
    console.log(response);
    this.uploaded_file_list = response.Detail.data;
    
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '6rem',
    // minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],

    toolbarPosition: 'top',



    toolbarHiddenButtons: [
      [
        // 'undo',
        // 'redo',
        // 'bold',
        // 'italic',
        // 'underline',
        'strikeThrough',
        'subscript',
        'superscript',
        // 'justifyLeft',
        // 'justifyCenter',
        // 'justifyRight',
        // 'justifyFull',
        'indent',
        'outdent',
        'insertUnorderedList',
        'insertOrderedList',
        'heading',
        // 'fontName'
      ],
      [
        // 'fontSize',
        // 'textColor',
        'backgroundColor',
        'customClasses',
        // 'link',
        'unlink',
        // 'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        // 'toggleEditorMode'
      ]
    ]


  };

}
