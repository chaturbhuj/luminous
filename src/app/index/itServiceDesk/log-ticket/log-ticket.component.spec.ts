import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogTicketComponent } from './log-ticket.component';

describe('LogTicketComponent', () => {
  let component: LogTicketComponent;
  let fixture: ComponentFixture<LogTicketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogTicketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
