import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RightMasterComponent } from './right-master.component';

describe('RightMasterComponent', () => {
  let component: RightMasterComponent;
  let fixture: ComponentFixture<RightMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RightMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RightMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
