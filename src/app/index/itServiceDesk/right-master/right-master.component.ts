import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';


import { RightMasterServiceService } from 'src/app/services/itServiceDesk/right-master-service.service';

@Component({
  selector: 'app-right-master',
  templateUrl: './right-master.component.html',
  styleUrls: ['./right-master.component.css']
})
export class RightMasterComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService, private service: RightMasterServiceService, private itServiceDesk: ItServiceDeskService,) { }
  filterVal: any= '';
  tabledata: any;
  p: number = 1;
  collection: any[] = [];

  ngOnInit(): void {
    this.applyFilter();
  }

  async applyFilter() {
    console.log("asdfg")
    this.spinner.show();
    const res: any = await this.service.getuserrightmaster(this.filterVal)
    this.spinner.hide();
    this.tabledata = res.Detail.data;

  }
  // async filter() {

  // }

}