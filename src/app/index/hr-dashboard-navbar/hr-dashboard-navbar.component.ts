import { Component, OnInit,Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router, Routes } from '@angular/router';

@Component({
  selector: 'app-hr-dashboard-navbar',
  templateUrl: './hr-dashboard-navbar.component.html',
  styleUrls: ['./hr-dashboard-navbar.component.css']
})
export class HrDashboardNavbarComponent implements OnInit {

  @Output() page_link = new EventEmitter<number>();


  constructor(private activatedRoute: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
  }

  go_to_link(){
    this.router.navigate(['loanAdvanceRequest'], {relativeTo:  this.activatedRoute});
  }
  go_to_link1(){
    this.router.navigate(['loanAdvanceApproval'], {relativeTo: this.activatedRoute});
  }
}
