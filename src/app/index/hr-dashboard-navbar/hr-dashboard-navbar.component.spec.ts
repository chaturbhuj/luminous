import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HrDashboardNavbarComponent } from './hr-dashboard-navbar.component';

describe('HrDashboardNavbarComponent', () => {
  let component: HrDashboardNavbarComponent;
  let fixture: ComponentFixture<HrDashboardNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HrDashboardNavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HrDashboardNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
