import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DashboardServiceService } from 'src/app/services/dashboard/dashboard-service.service';
import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';
import { OwlOptions } from 'ngx-owl-carousel-o';
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private dashboardService: DashboardServiceService, private spinner: NgxSpinnerService, private router:Router) { }
  dashboardImages: any;
  dashboardImagesclick: any;
  banner_image =''
  birthday_list:any=[]
  abt_our_cmp_list:any =[]
  about_cmp_detail:any =[]
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 600,
    navText: ['&#8249', '&#8250;'],
    responsive: {
      0: {
        items: 1 
      },
      400: {
        items: 2
      },
      760: {
        items: 3
      },
      1000: {
        items: 4
      }
    },
    nav: true
  }
  
  openModal(){
    $('#createSession').modal('show');
  }
  ngOnInit(): void {
    this.getDashboardHomeImages();
    // this.getDashboardHomeClick();
    this.getDashboardBannerImage();
    this.getBirthdayList();
    this.getAboutOurCompany();

  }

  logout(){
    localStorage.clear();
    this.router.navigateByUrl('login');
  }

  async getDashboardHomeImages() {
    this.spinner.show();
    const res: any = await this.dashboardService.getDashboardHomeImages();
    this.dashboardImages = res.Detail.data;
    // console.log(res);
    this.spinner.hide();
  }

  async getDashboardHomeClick(mid:any) {
    this.openModal();
    this.spinner.show();
    const res: any = await this.dashboardService.getDashboardClick(mid);
    this.dashboardImagesclick = res.Detail.data;
    this.spinner.hide();
    console.log(res);
  }

  async getDashboardBannerImage(){
    this.spinner.show();
    const res: any = await this.dashboardService.getDashboardBannerImage();
    console.log(res);
    this.banner_image = res.Detail.data[0].imgPath;
    this.spinner.hide()
  }

  async getBirthdayList(){
    this.spinner.show();
    const res: any = await this.dashboardService.getBirthdayList();
    console.log(res);
    this.birthday_list = res.Detail.data;
    console.log(this.birthday_list);
    
    this.spinner.hide()
  }

  async getAboutOurCompany(){
    this.spinner.show();
    let data ={
      Appname:'dashboard'
    }
    const res: any = await this.dashboardService.Dash_Category_Hdr(data);
    console.log(res);
    this.abt_our_cmp_list = res.Detail.data;
    console.log(this.abt_our_cmp_list);
    
    this.spinner.hide()
  }
 
  async about_company(cat_id:any,action:any,pageName:any){
    console.log("dfdfdf");
    
    if(action=='FD'){
      this.spinner.show();
      let data ={
        CatId:cat_id
      }
      const res: any = await this.dashboardService.Dash_Category_Det(data);
      console.log(res);
       this.about_cmp_detail = res.Detail.data[0];
       console.log(this.about_cmp_detail);
      
      this.spinner.hide()
      console.log(res);
      window.open(this.about_cmp_detail.DocPath)
    }

    if(action == 'NP'){ 
      console.log(pageName);
      this.router.navigate(['/pages/about-company'], { queryParams: {cat_id: cat_id}});
    }
   
  }

  change_password(){
    this.router.navigate(['/pages/change-password'])
  }


}
