import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ItServiceDeskService } from 'src/app/services/itServiceDesk/it-service-desk.service';
import { AuthenticationResult } from '@azure/msal-browser';
import { MsalService } from '@azure/msal-angular';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {

  login: FormGroup;
  otpForm: FormGroup;
  changePasswordForm: FormGroup;
  constructor(private mauthService: MsalService, private user: AuthService, private fb: FormBuilder, private router: Router, private itService:ItServiceDeskService) { }

  flag:string = 'login';
  timerVar = 60;
  intervalId:any;
  showTimer: Boolean = false;

  ngOnInit(): void {

    this.login = this.fb.group({
      userid: ['', Validators.required],
      pass: ['', Validators.required]
    })

    this.otpForm = this.fb.group({
      otp: ['', Validators.required]
    })

    this.changePasswordForm = this.fb.group({
      userid: ['', Validators.required],
      oldpwd: ['', Validators.required],
      newpwd: ['', Validators.required],
      confirmpwd: ['', Validators.required]
    })
  }

  miclogin() {
	  this.mauthService.loginPopup()
      .subscribe((response: AuthenticationResult) => {
		  console.log(response);
        this.mauthService.instance.setActiveAccount(response.account);
      });
  }
  ngOnDestroy() {
    clearInterval(this.intervalId)
  }

  get userid() {
    return this.login.get('userid');
  }

  get userid2() {
    return this.changePasswordForm.get('userid');
  }

  get pass() {
    return this.login.get('pass');
  }

  get otp() {
    return this.otpForm.get('otp');
  }

  get newpwd() {
    return this.changePasswordForm.get('newpwd');
  }

  get confirmpwd() {
    return this.changePasswordForm.get('confirmpwd');
  }


  async loginSubmit() {

    this.login.markAllAsTouched();

    if (this.login.invalid) {
      console.log(this.pass)
      console.log(this.userid)
      return;
    }

    try {
      const res: any = await this.user.login(this.login.value)
      console.log(res)
      if (res.status == 200) {
        localStorage.setItem('auth_key', res.Detail.key);
        localStorage.setItem('userid', res.Detail.userid);
        this.checkTechnician();
        this.router.navigateByUrl('/dashboard');
      }
      else {
        alert(res.Message)
      }
    } catch (error) {
      console.log(error)
    }
  }

  async sendOtp(){
    this.userid?.markAllAsTouched()
    if(this.userid?.hasError('required')){
      return;
    }
    try {
      let formData = {
        userid:this.userid?.value
      }
      this.showTimer = true;
      this.intervalId = setInterval(() => {
        this.timerVar--;
        if (this.timerVar == 0) {
          clearInterval(this.intervalId);
          this.timerVar = 60
          this.showTimer = false;
        }
      }, 1000)
      const res:any = await this.user.forgetPassword(formData)
      if(res.status == 200){
        // console.log("if")
        alert(res.Message)
        clearInterval(this.intervalId);
        this.timerVar = 60
        this.showTimer = false;
        this.flag = 'enterOtp';
      }
      else{
        // console.log("else")
        alert(res.Message)
        clearInterval(this.intervalId);
        this.timerVar = 60
        this.showTimer = false;
      }
    } catch (error) {
      console.log(error)
    }
  }


  async submitOtp(){
    this.otp?.markAllAsTouched()
    if(this.otp?.hasError('required')){
      return;
    }
    try {
      let formData = {
        userid:this.userid?.value,
        otp:this.otp?.value
      }
      const res:any = await this.user.submitOtp(formData)
      if(res.status == 200){
        alert(res.Message)
        this.changePasswordForm.patchValue({
          userid:res.Detail.userid,
          oldpwd:res.Detail.oldpwd
        })
        this.flag = "changePassword"
      }
      else{
        alert(res.Message)
      }
    } catch (error) {
      console.log(error)
    }
  }

  async changePassword(){
    this.changePasswordForm.markAllAsTouched()
    if(this.changePasswordForm.invalid){
      return;
    }
    if(this.newpwd?.value != this.confirmpwd?.value){
      return;
    }
    try {
      const res:any = await this.user.changePassword(this.changePasswordForm.value)
      if(res.status == 200){
        alert(res.Message)
        this.ngOnInit();
      }
      else{
        alert(res.Message)
      }
    } catch (error) {
      console.log(error)
    }
  }

  changeFlag(val:any){
    this.login.markAsUntouched();
    this.flag = val;
  }

  async checkTechnician(){
    try {
      // const res2:any = await this.itService.checkTech2();
      const res:any = await this.itService.checkTech();
      console.log(res)
      if(res.Message == "success"){
        localStorage.setItem('technician', "true");
      }else{
        localStorage.setItem('technician', "false");
      }
    } catch (err) {
      console.log(err)
    }
  }


}
