import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuardGuard } from './guards/login-guard.guard';
import { LogoutGuardGuard } from './guards/logout-guard.guard';
import { HrdashboardComponent } from './index/hrDashboard/hrdashboard/hrdashboard.component';
import { DashboardComponent } from './index/dashboard/dashboard.component';
import { FormsComponent } from './index/forms/forms.component';
import { LoanAdvanceRequestComponent } from './index/hrDashboard/loan-advance-request/loan-advance-request.component';
import { AssignmentComponent } from './index/itServiceDesk/assignment/assignment.component';
import { HomeComponent } from './index/itServiceDesk/home/home.component';
import { MainComponent } from './index/itServiceDesk/main/main.component';
import { ProjectComponent } from './index/itServiceDesk/project/project.component';
import { RightMasterComponent } from './index/itServiceDesk/right-master/right-master.component';
import { ShowTicketDetailsComponent } from './index/itServiceDesk/show-ticket-details/show-ticket-details.component';
import { TechTicketDetailComponent } from './index/itServiceDesk/tech-ticket-detail/tech-ticket-detail.component';
import { SideNavComponent } from './index/side-nav/side-nav.component';

import { UserloginComponent } from './index/userlogin/userlogin.component';
import { LoanAdvanceApprovalComponent } from './index/hrDashboard/loan-advance-approval/loan-advance-approval.component';
import { LoanPendingApprovalComponent } from './index/hrDashboard/loan-pending-approval/loan-pending-approval.component';
import { DashboardMenuComponent } from './index/hrDashboard/dashboard-menu/dashboard-menu.component';
import { LoanAdvanceReportComponent } from './index/hrDashboard/loan-advance-report/loan-advance-report.component';
import { OwnAttendanceComponent } from './index/hrDashboard/own-attendance/own-attendance.component';
import { ReporteeAttendanceComponent } from './index/hrDashboard/reportee-attendance/reportee-attendance.component';
import { AttendanceHRComponent } from './index/hrDashboard/attendance-hr/attendance-hr.component';
import { EmpAttendanceMappingComponent } from './index/hrDashboard/emp-attendance-mapping/emp-attendance-mapping.component';  
import { AboutCompanyComponent } from './index/Pages/about-company/about-company.component';
import { ChangePasswordComponent } from './index/Pages/change-password/change-password.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: UserloginComponent,
    canActivate: [LogoutGuardGuard]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [LoginGuardGuard],
  },
  {
    path: 'sideNav',
    component: SideNavComponent,
  },
  {
    path: 'pages',
    children:[
      {
        path: 'about-company',
        component: AboutCompanyComponent,
        canActivate:[LoginGuardGuard]

      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
        canActivate:[LoginGuardGuard]

      },
    ]
  },
  {
    path: 'itServiceDesk',
    children: [
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'project',
        component: ProjectComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'main',
        component: MainComponent,
        canActivate: [LoginGuardGuard],
      },

      {
        path: 'showTicketDetail/:id',
        component: ShowTicketDetailsComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'rightsmaster',
        component: RightMasterComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'assignment',
        component: AssignmentComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'techTicketDetails/:type',
        component: TechTicketDetailComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'techTicketDetails',
        component: TechTicketDetailComponent,
        canActivate: [LoginGuardGuard],
      }
    ]

  },

  {
    path: 'hrDashboard',
    children: [
      {
        path: '',
        component: DashboardMenuComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'loanAdvanceRequest',
        component: LoanAdvanceRequestComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'loanAdvanceApproval',
        component: LoanAdvanceApprovalComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'loanPendingApproval',
        component: LoanPendingApprovalComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'loanAdvanceReport',
        component: LoanAdvanceReportComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'ownAttendance',
        component: OwnAttendanceComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'reporteeAttendance',
        component: ReporteeAttendanceComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'AttendanceHR',
        component: AttendanceHRComponent,
        canActivate: [LoginGuardGuard],
      },
      {
        path: 'EmpAttendanceMapping',
        component: EmpAttendanceMappingComponent,
        canActivate: [LoginGuardGuard],
      },
  
    ]
  },
  
 

  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
